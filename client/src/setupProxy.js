const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(
        '/api',
        createProxyMiddleware({
            target: process.env.REACT_APP_SERVER,
            //target: 'http://tube-lite.ddns.com:5000',
            //target: 'http://192.168.43.128:5000',
            changeOrigin: true,
        })
    );
};