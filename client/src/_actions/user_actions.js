import axios from 'axios';
import {
    LOGIN_USER,
    REGISTER_USER,
    AUTH_USER,
    LOGOUT_USER,
    RESET_USER
} from './types';
import { USER_SERVER } from '../components/Config.js';

export function registerUser(dataToSubmit){
    const request = axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/users/register`,dataToSubmit)
        .then(response => response.data);
    
    return {
        type: REGISTER_USER,
        payload: request
    }
}

export function resetUser(dataToSubmit){
    const request = axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/users/reset`,dataToSubmit)
        .then(response => response.data);
    
    return {
        type: RESET_USER,
        payload: request
    }
}

export function loginUser(dataToSubmit){
    const request = axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/users/login`,dataToSubmit)
                .then(response => response.data);

    return {
        type: LOGIN_USER,
        payload: request
    }
}

export function auth(token = null){
    console.log("token: ",token)
    const request = axios.get(`${process.env.REACT_APP_SERVER}/api/users/auth`,{
        headers:{
            Authorization: 'Bearer '+ token
        }
    }).then(response => response.data);

    return {
        type: AUTH_USER,
        payload: request
    }
}

export function logoutUser(token){
    const request = axios.get(`${USER_SERVER}/logout`,{
        headers:{
            Authorization: 'Bearer ' +token
        }
    })
    .then(response => response.data);

    return {
        type: LOGOUT_USER,
        payload: request
    }
}

