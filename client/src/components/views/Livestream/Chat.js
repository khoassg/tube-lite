import React,{useState,useEffect} from 'react';
import {Button, Empty, Input, message,Icon,Typography} from 'antd';
import axios from 'axios';
import SingleChat from './SingleChat';
import {client} from '../../Config';


const {Text} = Typography;
const {TextArea} = Input;

function Chat(props) {
    const token = localStorage.getItem('token')
    const userId = localStorage.getItem('userId')
    const userName = localStorage.getItem('user_name')
    const [CommentLists, setCommentLists] = useState([])
    const [flagChat, setFlagChat] = useState(false)
    const [Comment, setComment] = useState("")
    const [count, setCount] = useState(1)
    const updateComment = (newComment) => {
        setCommentLists(CommentLists.concat(newComment))
    }
    const handleChange = (e) => {
        setComment(e.currentTarget.value)
    }
    const handleKeyDown = (e) =>{
        if(e.key === 'Enter'){
            onSubmit(e)
        }
    }
    const onSubmit =(e) => {
        e.preventDefault()
        if(!userId){
            message.warning("First, you have to log in!")
            return;
        }
        if(Comment === ''){
            message.info("You have to text something to comment on this video!")
            return;
        }
        const variables = {
            content: Comment,
            writer: userId,
            //postId:props.postId,
            roomId: props.roomId
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/comment/saveComment`, variables,{
            headers:{
                Authorization:'Bearer '+token
            }
        })
        .then(response=>{
            if(response.data.success){
                setComment("")
                updateComment(response.data.comment)
            }else{
                alert('Failed to save this comment!')
            }
        })
    }

    const handleLeftRoom = () =>{
        client.send(JSON.stringify({
            command:'left_room',
            room_id:props.roomId,
            name: userName
        }))
    }

    useEffect(() => {
        const videoVariable = {
            roomId: props.roomId
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/comment/getComments`,videoVariable)
        .then(response=>{
            if(response.data.success){
                console.log(response.data.comments)
                setCommentLists(response.data.comments)
            }else{
                message.error("Failed to get comments information !")
            }
        })
        axios.get(`${process.env.REACT_APP_SERVER}/api/livestream/room-viewers`,{
            params:videoVariable
        }).then(response=>{
            if(response.data.success){
                console.log(response.data.viewers)
                setCount(response.data.viewers)
            }else{
                message.error("Failed to get comments information !")
            }
        })
        client.onmessage = (message)=>{
            const dataFromServer = JSON.parse(message.data)
            if(dataFromServer.type === "chat"){
                let listTemp = [...CommentLists]
                listTemp.push(dataFromServer)
                setCommentLists(listTemp)
                setFlagChat(!flagChat)
            }
            else if(dataFromServer.type === "server_join_room"){
                setFlagChat(!flagChat)
            }
            else if(dataFromServer.type === "server_left_room"){
                setFlagChat(!flagChat)
            }
        }
    }, [flagChat])
    useEffect(() => {
        return () =>{
            handleLeftRoom()
        }
    }, [])
    return (
        <div>
            <p style={{textAlign:'center'}}><Icon type="eye" theme="twoTone" /> <Text strong>{count}</Text></p>
            <hr/>
            <div className="chatbox-web">
                <div className="livestream-chatbox">
                    {/* Comment Lists*/}
                    {CommentLists.length > 0 && CommentLists.map((comment,index)=>(
                        (!comment.responseTo && 
                            (comment.type !== 'notice-room' ?
                                <React.Fragment key={index}>
                                    <SingleChat comment={comment} postId={props.postId} refreshFunction={props.refreshFunction}/>
                                </React.Fragment>
                                :
                                <React.Fragment key={index}>
                                    <Text className="livestream-chat-text" disabled>{comment.content}</Text>
                                    <br/>
                                </React.Fragment>
                            )
                        )
                    ))}
                </div>
                {/* Root Comment Form*/}
                <form 
                    className = "livestream-chatform"
                    onSubmit={onSubmit}
                >
                    <TextArea
                        className = "livestream-text-area"
                        onChange={handleChange}
                        onKeyDown={handleKeyDown}
                        value ={Comment}
                        placeholder="Write some comments"
                    />
                    <Button 
                        className = "livestream-button"
                        type='primary' onClick = {onSubmit}
                    >
                        <Icon className="livestream-icon" type="right-square" theme="twoTone" />
                    </Button>
                </form>
            </div>
        </div>
    )
}

export default Chat
