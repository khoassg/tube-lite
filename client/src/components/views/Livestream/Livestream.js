import React from 'react'
import { Form, Typography,Input, Row,Button,message } from 'antd';
import { Formik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import {client} from "../../Config"

const { Title } = Typography;

const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

function Livestream(props) {
    const token = localStorage.getItem("token")
    const userId = localStorage.getItem("userId")
    const connection_id = localStorage.getItem("connection_id")
    return (
        <div style={{ width: '85%', margin: '0.6rem auto' }}>
            <Title level={2} > Livestream </Title>
            <hr />
            <Row>
                <Formik
                    initialValues={{
                        email: '',
                        password: localStorage.getItem("livestream_key"),
                        description: ''
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string()
                        .required('Title is required'),
                        password: Yup.string()
                        .required('Stream key is required'),
                        description: Yup.string()
                        .required('Title is required'),
                    })}
                    onSubmit={(values) => {
                        setTimeout(() => {
                            axios.post(`${process.env.REACT_APP_SERVER}/api/livestream/update-stream-setting`,{
                                live_on:1,
                                title: values.email,
                                description: values.description,
                                livestream_key:values.password
                            },{
                                headers:{
                                    Authorization: 'Bearer '+token
                                }
                            }).then((response)=>{
                                if(response.data.success){
                                    const room_id = response.data.roomId 
                                    client.send(JSON.stringify({
                                        command:'join_room',
                                        room_id: room_id,
                                        connection_id: connection_id,
                                        name: 'Host'
                                    }))
                                    props.history.push(`/livestream/video/${userId}/${room_id}`)
                                }else{
                                    message.error(response.data.err.message);
                                }
                            })
                        }, 500);
                    }}
                    >
                    {props => {
                        const {
                        values,
                        touched,
                        errors,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        } = props;
                        return (
                        <div className="livestream-form">
                            <Form style={{ minWidth: '320px' }} {...formItemLayout} onSubmit={handleSubmit} >
                            <Form.Item required label="Title" hasFeedback validateStatus={errors.email && touched.email ? "error" : 'success'}>
                                <Input
                                id="email"
                                placeholder="Enter your title"
                                type="email"
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                    errors.email && touched.email ? 'text-input error' : 'text-input'
                                }
                                />
                                {errors.email && touched.email && (
                                <div className="input-feedback">{errors.email}</div>
                                )}
                            </Form.Item>

                            <Form.Item label="Description">
                                <Input.TextArea
                                style={{height:'20%'}}
                                id="description"
                                placeholder="Description your livestream"
                                type="text"
                                value={values.description}
                                onChange={handleChange}
                                />
                            </Form.Item>
                            <Form.Item required label="Stream key" hasFeedback validateStatus={errors.password && touched.password ? "error" : 'success'}>
                                <Input.Password
                                id="password"
                                placeholder="Enter your stream key"
                                type="text"
                                value={values.password}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                    errors.password && touched.password ? 'text-input error' : 'text-input'
                                }
                                readOnly = {true}
                                />
                                {errors.password && touched.password && (
                                <div className="input-feedback">{errors.password}</div>
                                )}
                            </Form.Item>

                            <Form.Item {...tailFormItemLayout}>
                                <Button
                                    style={{width:'100%'}} 
                                    onClick={handleSubmit} 
                                    type="primary" 
                                    disabled={isSubmitting}
                                >
                                Next
                                </Button>
                            </Form.Item>
                            </Form>
                        </div>
                        );
                    }}
                    </Formik>
            </Row>
        </div>
    )
}

export default Livestream
