import React from 'react'
import {Comment, Avatar} from 'antd'
import Moment from 'moment'
// import Like from '../DetailVideoPage/Sections/Like'
// import Report from '../DetailVideoPage/Sections/Report'

function SingleChat(props) {
    const actions = [
        // <Like comment commentId={props.comment._id} userId={props.postId._id}/>,
        // <Report comment commentId={props.comment._id}/>    
    ]
    const author =[
        <React.Fragment key="author-basic">
            <p style={{display:'inline',fontWeight:'bold'}}>{props.comment.writer.name != null ? props.comment.writer.name : "Unknown"} </p>
            <p style={{display:'inline'}}>{Moment(props.comment.createdAt).format('LLL')}</p>
        </React.Fragment>
    ]
    return (
        <div>
                <Comment
                    className="livestream-comment-item"
                    actions={actions}
                    author={author}
                    avatar={
                        <Avatar src={props.comment.writer.image} alt="avatar"/>
                    }
                    content={
                        <p>
                            {props.comment.content}<br/>
                        </p>
                    }
                >
                </Comment>
        </div>
    )
}

export default SingleChat
