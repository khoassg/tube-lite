import React,{useEffect,useState} from 'react'
import { Card, Avatar, Col, Typography, Row,message } from 'antd';
import axios from "axios";
import moment from 'moment';
import {client} from "../../Config"

const { Title } = Typography;
const { Meta } = Card;
function ListLivestream(props) {
    const [Videos, setVideos] = useState([])
    const connection_id = localStorage.getItem("connection_id")
    const userName = localStorage.getItem("user_name")
    useEffect(()=>{
            axios.get(`${process.env.REACT_APP_SERVER}/api/livestream/list`)
            .then(response=>{
                if(response.data.success){
                    setVideos(response.data.videos)
                }else{
                    message.error("Failed to get videos in server!")
                }
            })
    },[])

    const handleJoinLivestream = (video) =>{
        const room_id = video.livestream_room 
        client.send(JSON.stringify({
            command:'join_room',
            room_id: room_id,
            connection_id: connection_id,
            name: userName ? userName: 'Guest'
          }))
        props.history.push(`/livestream/video/${video.writer._id}/${room_id}`)
    }

    const renderCards = Videos.map((video, index) => {

        // var minutes = Math.floor(video.duration / 60);
        // var seconds = Math.floor(video.duration - minutes * 60);

        return <Col key = {index} lg={5} md={8} xs={24} className = "menu__landingpage">
            <div style={{ position: 'relative',cursor:'pointer' }} onClick={()=>handleJoinLivestream(video)}>
                <img style={{ width: '100%' }} alt="thumbnail" src={`${video.writer.image}`} />
                <div className=" duration"
                    style={{ bottom: 0, right:0, position: 'absolute', margin: '4px', 
                    color: '#fff', backgroundColor: 'rgba(17, 17, 17, 0.8)', opacity: 0.8, 
                    padding: '2px 4px', borderRadius:'2px', letterSpacing:'0.5px', fontSize:'12px',
                    fontWeight:'500', lineHeight:'12px' }}>
                    <span>Live now!</span>
                </div>
            </div><br />
            <Meta
                avatar={
                    <Avatar src={video.writer.image} />
                }
                title={video.title}
            />
            <span>{video.writer.name} </span><br />
            Updated at<span> {moment(video.createdAt).format("LLL")} </span>
        </Col>

    })

    return (
        <div style={{ width: '85%', margin: '0.6rem auto' }}>
            <Title level={2} > List Livestream </Title>
            <hr />
            <Row>
                {Videos !== [] ? renderCards:
                (<div style={{fontSize:'20px',fontStyle:'oblique'}}> 
                    <p>No livestream available right now!</p>
                </div>)}
            </Row>
        </div>
    )
}

export default ListLivestream
