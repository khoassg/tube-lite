import React,{useEffect,useState} from 'react'
import { List, Typography, Row, Col, Avatar, message, Button } from 'antd'
import Chat from "./Chat";
import axios from "axios";
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import ReactHlsPlayer from 'react-hls-player';

const { Title } = Typography
function VideoLivestream(props) {
    const storageId = localStorage.getItem('userId')
    const connection_id = localStorage.getItem("connection_id")
    const userName = localStorage.getItem("user_name")
    const token = localStorage.getItem("token")
    const userId = props.match.params.userId
    const roomId = props.match.params.roomId
    const [user, setUser] = useState({})
    const [isEnd, setIsEnd] = useState(false)
    const description = (user) =>{
        return (
            <span> {user.livestream_description} 
                <br/> 
                Upload by {user.name} at {moment(user.createdAt).format("LLL")} 
            </span>
        )
    }
    const handleOffLivestream = () =>{
        console.log("Send ws off livestream")
        axios.post(`${process.env.REACT_APP_SERVER}/api/livestream/off-stream`,{
            name:localStorage.getItem("livestream_key")
        },{
            headers:{
                Authorization: "Bearer "+localStorage.getItem("token")
            }
        }).then(response=>{
            if(response.data.success){
                message.success("Off-stream successfully!")
            }else{
                message.error("Failed to off-stream")
            }   
        })
    }

    const handleBackDashboard = () =>{
        // props.history.push("/")
        setIsEnd(true)
    }

    useEffect(() => {
        //Clear previous interval
        if(interval != undefined && interval != 'undefined'){
            window.clearInterval(interval);
            console.log('Timer cleared with id'+interval);
        }
        if(userId){
            // Call api every 5 seconds
            var interval = setInterval(()=>{
                axios.get(`${process.env.REACT_APP_SERVER}/api/livestream/check-live-stream`,{
                    headers:{
                        Authorization:'Bearer '+token
                    },
                    params:{
                        user_id: userId
                    }
                }).then((response)=>{
                    if(response.data.success){
                        if (response.data.live === 0){
                            window.clearInterval(interval)
                            message.warning("This livestream has been ended by host!",5)
                            .then(()=>handleBackDashboard())
                        }
                    }
                }).catch(err=>{
                    window.clearInterval(interval)
                    message.warning("This livestream has been ended by host!",5)
                    .then(()=>handleBackDashboard())
                })
            },5000)
        }
        axios.get(`${process.env.REACT_APP_SERVER}/api/livestream/getVideo`,{
            params:{
                userId: userId
            }
        }).then(response=>{
            if(response.data.success){
                setUser(response.data.user)
            }else{
                message.error(response.data.err)
            }   
        })
    }, [])
    return (
        <div style={{ width: '95%', margin: '0.6rem auto' }}>
            <Title level={2} > Livestream </Title>
            <hr />
            <Row>
                <Col lg={18} xs={24}>
                    {isEnd === true ?
                        <img className="image-video-livestream" src="/end-livestream.jpg" alt="End livestream"/>
                        :
                        <ReactHlsPlayer className="livestream-hls-player" 
                            src={`${user.url}`}
                            controls = {true}
                            autoPlay = {true}
                        />
                    }
                    <br/>
                    <List.Item className="action__item"
                            actions={( userId === storageId &&
                                [
                                    <Button type="primary" onClick={()=>handleOffLivestream()}>Off livestream</Button>
                                ]
                            )}
                    >                
                        <List.Item.Meta className="action__livestream_meta"
                            avatar={<Avatar src={user.image} />}
                            title={<a href="#">{user.livestream_title}</a>}
                            description = {description(user)}
                        />
                    </List.Item>
                </Col>
                <Col lg={6} xs={24}>
                    <Chat postId={user} roomId={roomId}/>
                </Col>
            </Row>
        </div>
    )
}

export default withRouter(VideoLivestream)
