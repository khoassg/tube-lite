import React,{useEffect,useState} from 'react'
import { Card, Avatar, Col, Typography, Row,message } from 'antd';
import axios from "axios";
import moment from 'moment';


const { Title } = Typography;
const { Meta } = Card;
function ListRecordLivestream(props) {
    const [Videos, setVideos] = useState([])
    useEffect(()=>{
            axios.get(`${process.env.REACT_APP_SERVER}/api/video/getVideos`,{
                params:{
                    type:'Livestream'
                }
            })
            .then(response=>{
                if(response.data.success){
                    setVideos(response.data.videos)
                }else{
                    message.error("Failed to get videos in server!")
                }
            })
    },[])

    const handleJoinLivestream = (video) =>{
        const videoVariable = {
            videoId: video._id,
            owner_id: video.writer._id,
            userId: localStorage.getItem('userId')
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/video/clickView`,videoVariable)
            .then(response=>{
                if(response.data.success){
                    props.history.push(`/video/${video._id}`)
                }else{
                    message.error("Failed to click view video!")
                }
            })
    }
    const descriptionLivestream = (video) =>{
        return (
            <span>
                <span>{video.description} </span><br />
                <span style={{fontSize:'10px'}}>Updated at {moment(video.createdAt).format("LLL")} by {video.writer.name}</span>
            </span>
        )
    }

    const renderCards = Videos.map((video, index) => {

        // var minutes = Math.floor(video.duration / 60);
        // var seconds = Math.floor(video.duration - minutes * 60);

        return <Col key = {index} lg={5} md={8} xs={24} className = "menu__landingpage">
            <div style={{ position: 'relative',cursor:'pointer' }} onClick={()=>handleJoinLivestream(video)}>
                <img style={{ width: '100%' }} alt="thumbnail" src={`${video.writer.image}`} />
                <div className=" duration"
                    style={{ bottom: 0, right:0, position: 'absolute', margin: '4px', 
                    color: '#fff', backgroundColor: 'rgba(17, 17, 17, 0.8)', opacity: 0.8, 
                    padding: '2px 4px', borderRadius:'2px', letterSpacing:'0.5px', fontSize:'12px',
                    fontWeight:'500', lineHeight:'12px' }}>
                    <span>{video.duration}</span>
                </div>
            </div><br />
            <Meta
                avatar={
                    <Avatar src={video.writer.image} />
                }
                title={video.title}
                description={descriptionLivestream(video)}
            />
        </Col>

    })

    return (
        <div style={{ width: '85%', margin: '0.6rem auto' }}>
            <Title level={2} > List Record Livestream </Title>
            <hr />
            <Row>
                {Videos !== [] ? renderCards:
                (<div style={{fontSize:'20px',fontStyle:'oblique'}}> 
                    <p>No livestream available right now!</p>
                </div>)}
            </Row>
        </div>
    )
}

export default ListRecordLivestream
