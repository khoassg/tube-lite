import React,{useEffect,useState} from 'react';
import axios from 'axios';
import {message} from 'antd'

function Subscriber(props) {
    const token = localStorage.getItem('token');
    const userTo = props.userTo;
    const userFrom = props.userFrom;
    const [subscriberNumber, setSubscriberNumber] = useState(0)
    const [Subscribed, setSubscribed] = useState(true)
    const subscribeVariables ={
        'userTo': userTo,
        'userFrom': userFrom
    }
    const onSubscribe = () =>{
        if(!subscribeVariables.userFrom){
            message.warning("First, you have to log in!");
            return 0;
        }
        if(Subscribed){
            axios.post(`${process.env.REACT_APP_SERVER}/api/subscribe/unSubscribe`,subscribeVariables,{
                headers:{
                    Authorization: 'Bearer '+token
                }
            })
            .then(response => {
                if(response.data.success){
                    setSubscriberNumber(subscriberNumber - 1)
                    setSubscribed(!Subscribed)
                }else{
                    message.error("Failed to subscribe")
                }
            })
        }else{
            axios.post(`${process.env.REACT_APP_SERVER}/api/subscribe/subscribe`,subscribeVariables,{
                headers:{
                    Authorization: 'Bearer '+token
                }
            })
            .then(response => {
                if(response.data.success){
                    setSubscriberNumber(subscriberNumber + 1)
                    setSubscribed(!Subscribed)
                }else{
                    message.error("Failed to subscribe")
                }
            })
        }
    }
    useEffect(() => {
        const subscribeNumberVariables = {
            'userTo': userTo,
            'userFrom': userFrom
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/subscribe/subscribeNumber`,subscribeNumberVariables,{
            headers:{
                Authorization: 'Bearer '+token
            }
        })
        .then(response =>{
            if(response.data.success){
                console.log(response.data.subscribeNumber)
                setSubscriberNumber(response.data.subscribeNumber)
            }else{
                message.error('Failed to get subcriber number!')
            }
        })
        axios.post(`${process.env.REACT_APP_SERVER}/api/subscribe/subscribed`,subscribeNumberVariables)
        .then(response =>{
            if(response.data.success){
                console.log(response.data.subscribed)
                setSubscribed(response.data.subscribed)
            }else{
                message.error('Failed to get subscribed information')
            }
        })
    }, [])
    return (
        <div className="detail__action">
            <button className="action__user" style={{backgroundColor:`${Subscribed ? '#AAAAAA': '#1E90FF'}`}}
                onClick={onSubscribe}
            >
               {subscriberNumber} {Subscribed ? 'Subscribed':'Subscribe'}
            </button>
        </div>
    )
}

export default Subscriber
