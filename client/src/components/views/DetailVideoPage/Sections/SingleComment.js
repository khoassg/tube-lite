import React,{useState} from 'react'
import {Comment, Avatar, Button, Input, message} from 'antd'
import axios from 'axios'
import {useSelector} from 'react-redux'
import Moment from 'moment'
import Like from './Like.js'
import Report from './Report.js'

const {TextArea} = Input;
function SingleComment(props) {
    const token = localStorage.getItem('token')
    const user = useSelector(state => state.user.userData)
    const [CommentValue, setCommentValue] = useState("")
    const [OpenReply, setOpenReply] = useState(false)

    const handleChange = (e) =>{
        setCommentValue(e.currentTarget.value)
    }

    const openReply = () =>{
        setOpenReply(!OpenReply)
    }
    const onSubmit = (e) =>{
        e.preventDefault()
        if(!user._id){
            message.warning("First, you have to log in!");
            return 0;
        }
        console.log(props.comment)
        const variables = {
            writer: user._id,
            postId: props.postId,
            responseTo: props.comment._id,
            content: CommentValue
        }

        axios.post(`${process.env.REACT_APP_SERVER}/api/comment/saveComment`, variables,{
            headers:{
                Authorization:'Bearer '+token
            }
        })
        .then(response =>{
            if(response.data.success){
                setCommentValue("")
                setOpenReply(!OpenReply)
                props.refreshFunction(response.data.comment)
            }else{
                message.error("Failed to save this comment !")
            }
        })
    }
    const actions = [
        <Like comment commentId={props.comment._id} userId={user._id}/>,
        <Report comment commentId={props.comment._id}/>,
        <span onClick={openReply} key="comment-basic-reply-to">
            Reply to 
        </span>     
    ]
    const author =[
        <React.Fragment key="author-basic">
            <p style={{display:'inline',fontWeight:'bold'}}>{props.comment.writer.name} </p>
            <p style={{display:'inline'}}>{Moment(props.comment.createdAt).format('LLL')}</p>
        </React.Fragment>
    ]
    return (
        <div>
                <Comment
                    actions={actions}
                    author={author}
                    avatar={
                        <Avatar src={props.comment.writer.image} alt="avatar"/>
                    }
                    content={
                        <p>
                            {props.comment.content}<br/>
                        </p>
                    }
                >
                </Comment>
            {OpenReply && <form style={{ display: 'flex' }} onSubmit={onSubmit}>
                    <TextArea 
                        style={{ width: '100%', borderRadius: '5px' }}
                        onChange={handleChange}
                        value={CommentValue}
                        placeholder="write some comments"
                    />
                    <Button className="comment__button" onClick={onSubmit}>Submit</Button>
                </form>
            }
        </div>
    )
}

export default SingleComment
