import React,{useEffect,useState} from 'react';
import axios from 'axios';
import {message} from 'antd';

function SideVideo() {
    const [SideVideos, setSideVideos] = useState([]);
    useEffect(() => {
        axios.get(`${process.env.REACT_APP_SERVER}/api/video/getVideos`)
        .then(response=>{
            if(response.data.success){
                setSideVideos(response.data.videos)
            }else{
                message.error("Failed to get videos in server!")
            }
        })
    }, [])
    const sideVideoItems = SideVideos.map((video,index) => {
        let minutes = Math.floor(video.duration / 60)
        let seconds = Math.floor(video.duration - minutes* 60)
        return (
            <div key= {index} className="side__video">
                <div className="side__image">
                    <a href={`/video/${video._id}`} style = {{color:'gray'}}>
                        <img width="100%" src ={`${process.env.REACT_APP_SERVER}/${video.thumbnail}`} alt="video"/>
                    </a>
                    <br/>
                    <div className="side__duration">
                        {minutes}:{seconds}
                    </div>
                </div>
                <div style={{width:"50%"}}>
                    <a href={`/video/${video._id}`} style = {{color:'gray'}}>
                        <span style={{fontSize:'1rem', color:'black'}}>{video.title}</span><br/>
                        <span>{video.writer.name}</span><br/>
                        <span>{video.views} views</span>
                    </a>
                </div>
            </div>
        )
    })
    return (
        <React.Fragment>
            <div style={{marginTop:'3rem'}}></div>
                {sideVideoItems}
        </React.Fragment>

    )
}

export default SideVideo
