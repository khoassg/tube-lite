import React,{useState} from 'react';
import {Button, Empty, Input, message} from 'antd';
import axios from 'axios';
import {useSelector} from 'react-redux';
import SingleComment from './SingleComment';
import ReplyComment from './ReplyComment';

const {TextArea} = Input;

function Comments(props) {
    const token = localStorage.getItem('token')
    const user = useSelector(state => state.user.userData)
    const [Comment, setComment] = useState("")
    const handleChange = (e) => {
        setComment(e.currentTarget.value)
    }
    const onSubmit =(e) => {
        e.preventDefault()
        if(!user._id ){
            message.warning("First, you have to log in!")
        }
        const variables = {
            content: Comment,
            writer: user._id,
            postId:props.postId
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/comment/saveComment`, variables,{
            headers:{
                Authorization:'Bearer '+token
            }
        })
        .then(response=>{
            if(response.data.success){
                setComment("")
                props.refreshFunction(response.data.comment)
            }else{
                alert('Failed to save this comment!')
            }
        })
    }
    return (
        <div>
            <br/>
            <p>replies</p>
            <hr/>
            {/* Comment Lists*/}
            {console.log(props.CommentLists)}
            {props.CommentLists && props.CommentLists.map((comment,index)=>(
                (!comment.responseTo && 
                    <React.Fragment key={index}>
                        <SingleComment comment={comment} postId={props.postId} refreshFunction={props.refreshFunction}/>
                        <ReplyComment CommentLists={props.CommentLists} postId={props.postId} parentCommentId={comment._id} refreshFunction={props.refreshFunction} />
                    </React.Fragment>
                )
            ))}
            {/* Root Comment Form*/}
            <form style={{display:'flex'}} onSubmit={onSubmit}>
                <TextArea
                    style={{width:'100%', borderRadius:'5px'}}
                    onChange={handleChange}
                    value ={Comment}
                    placeholder="Write some comments"
                />
                <br/>
                <Button style={{width:'20%',height:'52px'}} onClick = {onSubmit}>Submit</Button>
            </form>
        </div>
    )
}

export default Comments
