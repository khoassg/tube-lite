import React,{useState,useEffect} from 'react'
import {Tooltip,Icon,Popover,List,Radio, Button, message} from "antd";
import axios from 'axios';



function Report(props) {
    const [listReport, setListReport] = useState([])
    const [checkedContent, setCheckedContent] = useState("")
    const [checkedType, setCheckedType] = useState("")
    const token = localStorage.getItem('token');
    const [visible, setVisible] = useState(false);
    const hide = () =>{
        setVisible(false)
    }
    const send = () =>{
        if(token){
            let variables = {
                source: props.video ? 'video':'comment',
                content: checkedContent,
                type: checkedType,
                object_id: props.video ? props.videoId: props.commentId
            }
            axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/v1/survey/add-report`,variables,{
                headers:{
                    Authorization: 'Bearer '+token
                }
            }).then(response=>{
                if(response.data.error_code === 0){
                    message.success('Send your report successfully!')
                    setVisible(false)
                }
            })
        }else{
            message.warning("You have to login to report this video!")
        }
    }
    const handleVisibleChange = (visible)=>{
        if(token){
            setVisible(visible)
        }else{
            message.warning("You have to login to use this feature!")
        }
    }
    const handleCheckboxChange =(value)=>{
        setCheckedContent(value.content)
        setCheckedType(value.type)
    }

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_PHP_SERVER}/api/v1/survey/list-question`,{
            headers:{
                Authorization: 'Bearer '+token
            }
        }).then(response=>{
            setListReport(response.data.data);
        })
    }, [])
    const renderDataReport = listReport.map((value,index)=>{
        return (
            <List.Item key={index}>
                <Radio
                    value={index}
                    onChange = {()=>handleCheckboxChange(value)}
                >
                </Radio>&nbsp;
                {value.content}&nbsp;
                <Tooltip title={value.description}>
                    <Icon type="question-circle"/>
                </Tooltip>
            </List.Item>
        )        
    })
    const content = (
        <div>
            <List
                size = "small"
            >
                <Radio.Group >
                    {renderDataReport}
                </Radio.Group>
            </List>
            <br/>
            <Button onClick={hide} type="dashed">Close</Button>
            <Button style={{float:"right"}}onClick={send} type="primary">Send</Button>
        </div>  
    );
    return (
        <div className="action__report" >
            <React.Fragment>
                <span key="report">
                    <Popover
                        content={content}
                        title="Report video"
                        trigger="click"
                        visible={visible}
                        onVisibleChange={handleVisibleChange}
                    >
                        <Tooltip title="Report">
                            <Icon type="flag"/>
                        </Tooltip>
                    </Popover>
                </span>
            </React.Fragment>
        </div>
    )
}

export default Report
