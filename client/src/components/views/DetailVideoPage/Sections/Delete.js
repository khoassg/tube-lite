import React,{useState} from 'react'
import {message} from 'antd'
import ConfirmDialog from "./ConfirmDialog"
import axios from "axios"
function Delete(props) {
    const token = localStorage.getItem('token') 
    const [OpenDialog, setOpenDialog] = useState(false)
    const ShowDialog = () =>{
        setOpenDialog(true)
    }
    const variables = {
        videoId : props.VideoId
    }
    const DeletePost = () => {
        axios.post(`${process.env.REACT_APP_SERVER}/api/video/deleteVideo`, variables,{
            headers:{
                Authorization:'Bearer '+token
            }
        })
        .then(response => {
            if(response.data.success){
                message.success("Delete successfully").then(()=>props.isDelete())
            }else{
                message.error("Failed to delete this video!");
            }
        })
    }
    return (
        <div className="detail__action_delete">
            <button className="action__user" onClick = {ShowDialog}
            >
               Delete
            </button>
            <ConfirmDialog
                title="Delete Video?"
                open={OpenDialog}
                setOpen={setOpenDialog}
                onConfirm={DeletePost}
            >
                Are you sure you want to delete this video?
            </ConfirmDialog>
        </div>
    )
}

export default Delete
