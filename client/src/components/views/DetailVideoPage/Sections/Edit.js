import React from 'react'

function Edit(props) {
    return (
        <div className="detail__action_edit">
                <a href = {`/edit/${props.VideoId}`}>
                    <button className="action__user"
                    >
                        Edit
                    </button>
                </a>
        </div>
    )
}

export default Edit
