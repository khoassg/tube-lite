import React,{useEffect,useState} from 'react'
import {Tooltip, Icon, message} from 'antd'
import axios from 'axios'



function Like(props) {
    const token = localStorage.getItem('token')
    const [Likes, setLikes] = useState(0)
    const [Dislikes, setDislikes] = useState(0)
    const [LikeAction, setLikeAction] = useState(null)
    const [DislikeAction, setDislikeAction] = useState(null)
    
    let variable = {};
    if(props.video){
        variable = {videoId:props.videoId,userId:props.userId,owner_id:props.owner_id}
    }else{
        variable = {commentId:props.commentId,userId:props.userId,owner_id:props.owner_id}
    }

    useEffect(() => {
        axios.post(`${process.env.REACT_APP_SERVER}/api/like/getLikes`,variable)
        .then(response =>{
            if(response.data.success){
                console.log(response.data)
                setLikes(response.data.likes.length)
                setDislikes(response.data.dislikes.length)

                response.data.likes.map(like=>{
                    if(like.userId === props.userId){
                        setLikeAction('liked')
                    }
                })
                
                response.data.dislikes.map(dislike =>{
                    if(dislike.userId === props.userId){
                        setDislikeAction('disliked')
                    }
                })
            }else{
                message.error("Failed to get likes!")
            }
        })
    }, [])

    const onLike = () =>{
        if(!variable.userId){
            message.warning("First, you have to log in!");
            return 0;
        }
        if(LikeAction === null){
            variable['type'] = 'like'
            axios.post(`${process.env.REACT_APP_SERVER}/api/like/uplike`,variable,{
                headers:{
                    Authorization: 'Bearer '+token
                }
            })
            .then(response=>{
                if(response.data.success){
                    setLikes(Likes + 1)
                    setLikeAction('liked')
                    if(DislikeAction !== null){
                        setDislikeAction(null)
                        setDislikes(Dislikes-1)
                    }
                }else{
                    message.error("Failed to like/dislike!")
                }
            })
        }else{
            variable['type'] = 'like'
            axios.post(`${process.env.REACT_APP_SERVER}/api/like/unlike`,variable,{
                headers:{
                    Authorization: 'Bearer '+token
                }
            })
            .then(response=>{
                if(response.data.success){
                    setLikes(Likes-1)
                    setLikeAction(null)
                } else{
                    message.error("Failed to unlike/undislike !")
                }
            })
        }
    }
    const onDislike = () =>{
        if(!variable.userId){
            message.warning("First, you have to log in!");
            return 0;
        }
        if(DislikeAction === null){
            variable['type'] = 'dislike'
            axios.post(`${process.env.REACT_APP_SERVER}/api/like/uplike`,variable,{
                headers:{
                    Authorization: 'Bearer '+token
                }
            })
            .then(response=>{
                if(response.data.success){
                    setDislikes(Dislikes + 1)
                    setDislikeAction('disliked')
                    if(LikeAction !== null){
                        setLikeAction(null)
                        setLikes(Likes-1)
                    }
                }else{
                    message.error("Failed to like/dislike!")
                }
            })
        }else{
            variable['type'] = 'dislike'
            axios.post(`${process.env.REACT_APP_SERVER}/api/like/unlike`,variable,
            {
                headers:{
                    Authorization: 'Bearer '+token
                }
            })
            .then(response=>{
                if(response.data.success){
                    setDislikes(Dislikes-1)
                    setDislikeAction(null)
                } else{
                    message.error("Failed to unlike/undislike !")
                }
            })
        }
    }
    return (
        <div className="action__like">
            <React.Fragment>
                <span key="comment-basic-like">
                    <Tooltip title="Like">
                        <Icon type="like"
                            theme = {LikeAction === 'liked' ? 'filled':'outlined'}
                            onClick = {onLike}
                        />
                    </Tooltip>
                    <span style={{paddingLeft:'8px',cursor:'auto'}}>{Likes}</span>
                </span>&nbsp;&nbsp;
                <span key="comment-basic-dislike">
                    <Tooltip title="Dislike">
                        <Icon
                            type="dislike"
                            theme={DislikeAction === 'disliked' ? 'filled' : 'outlined'  } 
                            onClick = {onDislike}
                        />
                    </Tooltip>
                    <span style={{ paddingLeft: '8px', cursor: 'auto' }}>{Dislikes}</span>
                </span>&nbsp;&nbsp;
            </React.Fragment>
        </div>
    )
}

export default Like
