import React, { useEffect, useState } from "react";
import {List,Avatar,Typography,Row,Col} from "antd";
import axios from "axios";
import SideVideo from "./Sections/SideVideo";
import Subscriber from "./Sections/Subscriber";
import Comments from "./Sections/Comments";
import Like from "./Sections/Like";
import Edit from "./Sections/Edit";
import Delete from "./Sections/Delete";
import Report from "./Sections/Report";
import moment from 'moment';
import {useSelector} from "react-redux"
import { withRouter } from 'react-router-dom';

function DetailVideoPage(props){
    const user = useSelector(state => state.user.userData)
    const videoId = props.match.params.videoId
    const [Video,setVideo] = useState([])
    const [isDelete, setIsDelete] = useState(false)
    const [CommentLists, setCommentLists] = useState([])
    console.log(Video);
    const videoVariable = {
        videoId: videoId,
    }

    const setPushDelete = () => {
        setIsDelete(true)
    }
    const description = (Video) =>{
        return (
            <span> {Video.description} 
                <br/>
                {Video.writer.name} upload at {moment(Video.createdAt).format("MMM Do YY")} - {Video.views <= 1 ?   Video.views+ ' ' + ' view ': Video.views+ ' '+' views '} 
            </span>
        )
    }
    const handleClickWriter = (userId) =>{
        props.history.push(`/myChannel/${userId}`)
    }
    useEffect(() =>{

        axios.post(`${process.env.REACT_APP_SERVER}/api/video/getVideo`,videoVariable)
        .then(response=>{
            if(response.data.success){
                console.log(response.data.video)
                setVideo(response.data.video)
            }else{
                alert("Failed to get video infor")
            }
        })

        axios.post(`${process.env.REACT_APP_SERVER}/api/comment/getComments`,videoVariable)
        .then(response=>{
            if(response.data.success){
                console.log(response.data.comments)
                setCommentLists(response.data.comments)
            }else{
                alert("Failed to get comments information !")
            }
        })

    },[])
    const updateComment = (newComment) => {
        setCommentLists(CommentLists.concat(newComment))
    }
    if(isDelete){
        props.history.push("/")
    }
    if(Video.writer && user){
        return (
            <Row>
                <Col lg={18} xs={24}>
                    <div className="detail__post_page">
                         <video style={{ width: '100%' }} 
                            src={`${process.env.REACT_APP_SERVER}/${Video.filePath}`}
                            controls
                        >
                            {/* <source src = "http://27.74.246.80:82/hls/LoveStory.m3u8" 
                                type="application/x-mpegURL" 
                            /> */}
                        </video>
                        <List.Item className="action__item"
                            actions={(
                            Video.writer._id != localStorage.getItem('userId') ? 
                                [   
                                    <div className="action__like_subscribe">
                                        <Like video videoId={videoId} userId={user._id} owner_id={Video.writer._id}/>
                                        <Report video videoId={videoId}/>
                                        <Subscriber userTo={Video.writer._id} userFrom={localStorage.getItem('userId')}/>                                    
                                    </div>
                                ]: 
                                [   
                                    <div className="action__edit_delete">
                                        <Edit VideoId = {Video._id}/>
                                        <Delete VideoId = {Video._id} isDelete = {setPushDelete}/>
                                    </div>
                                ]
                            )}
                        >
                            <List.Item.Meta className="action__item_meta"
                                avatar={<Avatar style={{cursor:'pointer'}} src={Video.writer && Video.writer.image} onClick={()=>handleClickWriter(Video.writer._id)}/>}
                                title={<span style={{cursor:'pointer'}} onClick={()=>handleClickWriter(Video.writer._id)}>{Video.title}</span>}
                                description={description(Video)}
                            />
                        </List.Item>
                        <Comments CommentLists={CommentLists} postId = {Video._id} refreshFunction={updateComment}/>
                    </div>
                </Col>
                <Col lg={6} xs={24}>
                    <SideVideo />
                </Col>
            </Row>
    
        )
    }else{
        return(
            <div>Loading...</div>
        )
    }

}


export default withRouter(DetailVideoPage)