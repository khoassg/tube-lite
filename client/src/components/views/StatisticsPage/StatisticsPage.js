import React, { useEffect,useState } from 'react';
import {Typography,DatePicker, message, Pagination} from 'antd';
import {Doughnut, Bar} from 'react-chartjs-2';
import axios from 'axios';

const {Title} = Typography;

function StatisticsPage() {
  const token = localStorage.getItem('token')
  const [dataChart, setDataChart] = useState([])
  const [current, setCurrent] = useState(1)
  const [total, setTotal] = useState(0)
  const [fromDate, setFromDate] = useState("")
  const [toDate, setToDate] = useState("")
  const handleChangePage = (page) =>{
    setCurrent(page)
  }
  const handleChangeDate = (date,dateString)=>{
    console.log("Date: ",date)
    console.log("DateString: ",dateString)
    setFromDate(dateString[0])
    setToDate(dateString[1])
  }
  useEffect(() => {
    let data = {
      labels: [],
      datasets: [
        {
          label: 'Views',
          data: [],
          backgroundColor: 'rgb(255, 0, 102,0.3)',
          borderColor:'rgb(255, 0, 102,1)',
          hoverOffset: 4,
          borderWidth: 1
        },
        {
          label: 'Likes',
          data: [],
          backgroundColor: 'rgb(0,153,255,0.3)',
          borderColor:'rgb(0,153,255,1)',
          hoverOffset: 4,
          borderWidth: 1
        },
        {
          label: 'Dislikes',
          data: [],
          backgroundColor: 'rgb(255,0,0,0.3)',
          borderColor:'rgb(255,0,0,1)',
          hoverOffset: 4,
          borderWidth: 1
        },
        {
          label: 'Comments',
          data: [],
          backgroundColor: 'rgb(51,204,51,0.3)',
          borderColor:'rgb(51,204,51,1)',
          hoverOffset: 4,
          borderWidth: 1
        },
      ]
    }
    axios.get(`${process.env.REACT_APP_PHP_SERVER}/api/v1/report/report`,{
      headers:{
        Authorization: 'Bearer '+token
      },
      params:{
        page: current,
        from_date: fromDate,
        to_date: toDate
      }
    }).then(response=>{
      if(response.data.error_code === 0){
        response.data.data.items.map((value)=>{
          data.labels.push(value.label)
          data.datasets[0].data.push(value.views)
          data.datasets[1].data.push(value.likes)
          data.datasets[2].data.push(value.dislikes)      
          data.datasets[3].data.push(value.comments)
        })
        setDataChart(data);
        setTotal(response.data.data.meta.total)
      }else{
        message.error("Failed to get data statistics!")
      }
    })
  }, [current,toDate])
    return (
        <div style={{ width: '85%', margin: '0.6rem auto' }}>
            <Title level={2} > Statistics </Title>
            <hr />
            <div >
              <DatePicker.RangePicker style={{float:'right'}} size="small" onChange={handleChangeDate}/>
              <Bar data={dataChart}
                weight={80}
                height={300} 
                options ={{
                  maintainAspectRatio: false,
                  scales:{
                    yAxes:[
                      {
                        ticks:{
                          beginAtZero:true
                        }
                      }
                    ]
                  }
                }}
              />
              <Pagination style={{float:'right'}} size="small" current={current} total={total} onChange={handleChangePage}/>
              <br/>
            </div>
        </div>
    )
}

export default StatisticsPage
