import React,{useEffect,useState} from 'react'
import { Card, Avatar, Col, Typography, Row } from 'antd';
import axios from "axios";
import moment from 'moment';

const { Title } = Typography;
const { Meta } = Card;
function MyChannelPage(props) {
    const [Videos, setVideos] = useState([])
    const userId = props.match.params.userId
    useEffect(()=>{
            axios.get(`${process.env.REACT_APP_SERVER}/api/video/getChannel`,{
                params:{
                    writer:userId
                }
            })
            .then(response=>{
                if(response.data.success){
                    setVideos(response.data.videos)
                }else{
                    alert("Failed to get videos in server!")
                }
            })
    },[])

    const renderCards = Videos.map((video, index) => {

        if(video.category !== "Livestream"){    
            var minutes = Math.floor(video.duration / 60);
            var seconds = Math.floor(video.duration - minutes * 60);
        }

        return <Col key = {index} lg={5} md={8} xs={24} className = "menu__landingpage">
            <div style={{ position: 'relative' }}>
                <a href={`/video/${video._id}`}>
                    {video.category !== "Livestream"? 
                        <img style={{ width: '100%' }} alt="thumbnail" src={`${process.env.REACT_APP_SERVER}/${video.thumbnail}`} />
                        :<img style={{ width:'235px',height:'176px' }} alt="thumbnail" src={`${video.writer.image}`} />
                    }
                    <div className=" duration"
                        style={{ bottom: 0, right:0, position: 'absolute', margin: '4px', 
                        color: '#fff', backgroundColor: 'rgba(17, 17, 17, 0.8)', opacity: 0.8, 
                        padding: '2px 4px', borderRadius:'2px', letterSpacing:'0.5px', fontSize:'12px',
                        fontWeight:'500', lineHeight:'12px' }}>
                        {video.category !== "Livestream"?
                            <span>{minutes} : {seconds}</span>
                            :<span>{video.duration}</span>
                        }
                    </div>
                </a>
            </div><br />
            <Meta
                avatar={
                    <Avatar src={video.writer.image} />
                }
                title={video.title}
            />
            <span>{video.writer.name} </span><br />
            <span style={{ marginLeft: '3rem' }}> {video.views <= 1 ?   video.views+ ' ' + ' view': video.views+ ' '+' views'}  </span>
            - <span> {moment(video.createdAt).format("MMM Do YY")} </span>
        </Col>

    })

    return (
        <div style={{ width: '85%', margin: '0.6rem auto' }}>
            <Title level={2} > Channel </Title>
            <hr />
            <Row>
                {Videos !== [] ? renderCards:
                (<div style={{fontSize:'20px',fontStyle:'oblique'}}> 
                    <p>Let upload your first video!</p>
                </div>)}
            </Row>
        </div>
    )
}

export default MyChannelPage
