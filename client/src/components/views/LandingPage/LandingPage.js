import React,{useEffect,useState} from 'react'
import { FaCode } from "react-icons/fa";
import { Card, Avatar, Col, Typography, Row, message } from 'antd';
import axios from "axios";
import moment from 'moment';
import { useStore } from 'react-redux';
import { withRouter } from 'react-router-dom';

window.OneSignal = window.OneSignal || [];
const OneSignal = window.OneSignal;
const { Title } = Typography;
const { Meta } = Card;
function LandingPage(props) {
    const [Videos, setVideos] = useState([])
    const Keyword = props.match.params.keyword
    const navigateTab = (video)=>{
        const videoVariable = {
            videoId: video._id,
            owner_id: video.writer._id,
            userId: localStorage.getItem('userId')
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/video/clickView`,videoVariable)
            .then(response=>{
                if(response.data.success){
                    props.history.push(`/video/${video._id}`)
                }else{
                    message.error("Failed to click view video!")
                }
            })
    }
    useEffect(()=>{
        if(Keyword !=='all'&& Keyword !==''){
            const variable = {title: Keyword}
            axios.post(`${process.env.REACT_APP_SERVER}/api/video/search`,variable)
            .then(response=>{
                if(response.data.success){
                    setVideos(response.data.videos)
                }else{
                    alert("Failed to search video!")
                }
            })
        }else{
            axios.get(`${process.env.REACT_APP_SERVER}/api/video/getVideos`)
            .then(response=>{
                if(response.data.success){
                    
                    setVideos(response.data.videos)
                }else{
                    alert("Failed to get videos in server!")
                }
            })
        }
    },[Keyword])

    const renderCards = Videos.map((video, index) => {
        var minutes = Math.floor(video.duration / 60);
        var seconds = Math.floor(video.duration - minutes * 60);

        return <Col key = {index} lg={5} md={8} xs={24} className = "menu__landingpage">
            <div style={{ position: 'relative',cursor:'pointer' }} onClick={()=>navigateTab(video)}>
                    <img style={{ width: '100%' }} alt="thumbnail" src={`${process.env.REACT_APP_SERVER}/${video.thumbnail}`} />
                    <div className=" duration"
                        style={{ bottom: 0, right:0, position: 'absolute', margin: '4px', 
                        color: '#fff', backgroundColor: 'rgba(17, 17, 17, 0.8)', opacity: 0.8, 
                        padding: '2px 4px', borderRadius:'2px', letterSpacing:'0.5px', fontSize:'12px',
                        fontWeight:'500', lineHeight:'12px' }}>
                        <span>{minutes} : {seconds}</span>
                    </div>
            </div><br />
            <Meta
                avatar={
                    <Avatar src={video.writer.image} />
                }
                title={video.title}
            />
            <span>{video.writer.name} </span><br />
            <span style={{ marginLeft: '3rem' }}> {video.views <= 1 ?   `${video.views} view`: `${video.views} views`}  </span>
            - <span> {moment(video.createdAt).format("MMM Do YY")} </span>
        </Col>

    })
    return (
        <div style={{ width: '85%', margin: '0.6rem auto' }}>
            <Title level={2} > Recommended </Title>
            <hr />

            <Row>
                {Videos !== [] ? renderCards:
                (<div style={{fontSize:'20px',fontStyle:'oblique'}}> 
                    <p>No result for {Keyword}</p>
                </div>)}
            </Row>
        </div>
    )
}

export default withRouter(LandingPage)
