import React ,{useState, useEffect} from 'react';
import { Typography, Button, Form,message, Input, Icon, Progress } from 'antd';
import Dropzone from 'react-dropzone';
import {useSelector} from "react-redux";
import axios from'axios';
import { v4 as uuidv4 } from "uuid";
import {client} from '../../Config';
const { Title } = Typography;
const {Text} = Typography;
const { TextArea } = Input;
const Private = [
    {'value': 0, 'label':'Private'},
    {'value': 1,'label':'Public'}
];
const Catogory = [
    { value: 0, label: "Film & Animation" },
    { value: 0, label: "Autos & Vehicles" },
    { value: 0, label: "Music" },
    { value: 0, label: "Pets & Animals" },
    { value: 0, label: "Sports" },
];
const chunkSize = 1048576 * 50; //its 3MB, increase the number measure in mb
function UploadVideoPage(props){

    const user = useSelector(state => state.user);
    const token = localStorage.getItem('token');
    const user_id = localStorage.getItem('userId');
    const [title, setTitle] = useState("");
    const [Description,setDescription] = useState("");
    const [Privacy,setPrivacy] = useState(0);
    const [Categories,setCategories] = useState("Film & Animation");
    const [FilePath,setFilePath] = useState("");
    const [thumbnail,setThumbnail] = useState("");
    const [Duration,setDuration] = useState("");

    const [showProgress, setShowProgress] = useState(false);
    const [counter, setCounter] = useState(1);
    const [fileToBeUpload, setFileToBeUpload] = useState({});
    const [beginingOfTheChunk, setBeginingOfTheChunk] = useState(0);
    const [endOfTheChunk, setEndOfTheChunk] = useState(chunkSize);
    const [progress, setProgress] = useState(0);
    const [fileGuid, setFileGuid] = useState("");
    const [fileSize, setFileSize] = useState(0);
    const [chunkCount, setChunkCount] = useState(0);
    const [fileName, setFileName] = useState("");

    useEffect(() => {
        client.onmessage =(message) =>{
            const dataFromServer = JSON.parse(message.data)
            if(dataFromServer.upload_completed){
                //generate thumbnail with the file path
                let variable = {
                    filePath: dataFromServer.filePath,
                    fileName: fileName
                }
                console.log(variable)
                axios.post(`${process.env.REACT_APP_SERVER}/api/video/thumbnail`, variable,{
                    headers:{
                        Authorization: 'Bearer '+ token
                    }
                })
                .then(response =>{
                    if(response.data.success){
                        setThumbnail(response.data.thumbsFilePath)
                        setDuration(response.data.fileDuration)
                    }
                    else{
                        message.error('Failed to generate thumbnail in this video')
                    }
                })
            }
        }
        if (fileSize > 0) {
          setShowProgress(true);
          fileUpload(counter);
        }
    }, [fileToBeUpload, counter]);
    
    const handleChangeTitle = (event) => {
        
        setTitle(event.currentTarget.value);
    }
    const handleChangeDecsription = (event) => {
        setDescription(event.currentTarget.value);
    }
    const handleChangeOne = (event) => {
        console.log(event.currentTarget.value);
        setPrivacy(event.currentTarget.value);
    }
    const handleChangeTwo = (event) => {
        console.log(event.currentTarget.value);
        setCategories(event.currentTarget.value);
    }
    const onSubmit = (event)=>{
        event.preventDefault();
        if(user.userData && !user.userData.isAuth){
            message.warning("Please log in first")
        }
        if (title === "" || Description === "" ||
            Categories === "" || FilePath === "" ||
            Duration === "" || thumbnail === ""
        ) {
            return message.warning('Please first fill all the fields')
        }
        const variables = {
            writer: user.userData._id,
            title: title,
            description: Description,
            privacy: Privacy,
            filePath: FilePath,
            category: Categories,
            duration: Duration,
            thumbnail: thumbnail
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/video/uploadVideo`, variables,{
            headers:{
                Authorization:'Bearer ' + token
            }
        })
        .then(response => {
            if(response.data.success){
                message.success("Uploaded video success").then(()=>props.history.push('/'))
            }else{
                message.error("Failed to upload this video!");
            }
        })
    }

    const onDrop = (files)=>{
        const _file = files[0];
        console.log("file: ",_file);
        setFileSize(_file.size);
        setFileName(_file.name);
        const _totalCount =
        _file.size % chunkSize === 0
            ? _file.size / chunkSize
            : Math.floor(_file.size / chunkSize) + 1; // Total count of chunks will have been upload to finish the file
        setChunkCount(_totalCount);
        console.log('Total count: ',_totalCount);
        const _fileID = uuidv4() + "_" + _file.name;
        setFileGuid(_fileID);
        setFileToBeUpload(_file);
        // let formData = new FormData();
        // const config = {
        //     header:{'content-type':'multipart/form-data'}
        // }
        // console.log(files);
        // setLoading(!Loading);
        // console.log(Loading);
        // formData.append("file",files[0]);
        // axios.post(`${process.env.REACT_APP_SERVER}/api/video/uploadfiles`,formData,config)
        // .then(response=>{
        //     if(response.data.success){
                
        //     }else{
        //         message.error('Failed to save this video in server');
        //     }
        // })
    }

    const fileUpload = (counter) => {
        console.log("counter upload: ",counter);
        if (counter < chunkCount) {
          var chunk = fileToBeUpload.slice(beginingOfTheChunk, endOfTheChunk);
          uploadChunk(chunk);
        }else if(counter === chunkCount){
            var chunk = fileToBeUpload.slice(beginingOfTheChunk, endOfTheChunk);
            uploadCompleted(chunk);
        }
    };

    const uploadChunk = async (chunk) => {
        let formData = new FormData();
        let variables = {
            part: counter,
            total_parts: chunkCount,
            fileName: fileGuid,
        }
        for(var key in variables){
            formData.append(key,variables[key])
        }
        formData.append("file",chunk);
        try {
          const response = await axios.post(
            `${process.env.REACT_APP_SERVER}/api/video/uploadfiles`,
            formData,
            {
              headers: { 
                    'content-type':'multipart/form-data',
                     Authorization: 'Bearer ' + token
                },
            }
          );
          const data = response.data;
          if (data.success) {
            setBeginingOfTheChunk(endOfTheChunk);
            setEndOfTheChunk(endOfTheChunk + chunkSize);
            var percentage = Math.ceil((counter / chunkCount) * 100);
            setProgress(percentage);
            setCounter(counter + 1);
          } else {
            console.log("Error Occurred:", data.errorMessage);
          }
        } catch (error) {
          console.log("error", error);
        }
    };

    const uploadCompleted = async (chunk) => {
        let formData = new FormData();
        let variables = {
            part: counter,
            total_parts: chunkCount,
            fileName: fileGuid,
            is_final:1,
            user_id: user_id
        }
        for(var key in variables){
            formData.append(key,variables[key])
        }
        formData.append("file",chunk);
        console.log(formData);
        const response = await axios.post(
        `${process.env.REACT_APP_SERVER}/api/video/uploadfiles`,
          formData,
          {
            headers:{
                "Content-Type": "application/json",
                Authorization: 'Bearer '+token
            }
          }
        );
        const data = response.data;
        if (data.success) {
            setFileName(data.fileName);
            setFilePath(data.filePath);
            setProgress(100);
            setCounter(counter + 1);
        }else{
            message.error("Failed to upload video!")
        }
    };
    return (

        <div style={{ maxWidth: '700px', margin: '2rem auto' }}>
            <div style={{ textAlign: 'center', marginBottom: '2rem' }}>
                <Title level={2} > Upload Video</Title>
            </div>

            <Form onSubmit = {onSubmit}>
                <div  className = "upload__video">
                    {thumbnail === "" ? 
                        (!showProgress ?                     
                            <Dropzone onDrop = {onDrop}
                                multiple={false}
                                maxSize={20000000000}>
                                {({ getRootProps, getInputProps }) => (
                                    <div className="upload__ondrop"
                                        {...getRootProps()}
                                    >
                                        <input {...getInputProps()} />
                                        <Icon type="plus" style={{ fontSize: '3rem' }} />

                                    </div>
                                )}
                            </Dropzone> 
                            :                     
                            <div className='upload_progressbar'>
                                <Progress type="circle" percent={progress} />
                                <br/>
                                <br/>
                                {progress === 99 ?
                                    <Text type='warning'>Merging video...</Text>:''
                                }
                                {progress === 100 ?
                                    <Text type='warning'>Generating thumbnail...</Text>:''
                                }
                            </div>
                        )
                        :                    
                        <div className="upload__thumbnail">
                            <img src={`${process.env.REACT_APP_SERVER}/${thumbnail}`} alt="thumbnail" />
                        </div>
                    }

                </div>

                <br /><br />
                <div className = "upload__information">
                    <label>Title</label>
                    <Input
                        onChange={handleChangeTitle}
                        value={title}
                    />
                    <br /><br />
                    <label>Description</label>
                    <TextArea
                        onChange={handleChangeDecsription}
                        value={Description}
                    />
                    <br /><br />

                    <select onChange={handleChangeOne}>
                        {Private.map((item, index) => (
                            <option key={index} value={item.value}>{item.label}</option>
                        ))}
                    </select>
                    <br /><br />

                    <select onChange={handleChangeTwo}>
                        {Catogory.map((item, index) => (
                            <option key={index} value={item.label}>{item.label}</option>
                        ))}
                    </select>
                    <br /><br />

                    <Button className = "upload__button" type="primary" size="large" onClick={onSubmit}>
                        Submit
                    </Button>
                </div>
            </Form>
        </div>
    );
}
export default UploadVideoPage