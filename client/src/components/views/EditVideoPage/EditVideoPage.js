import React ,{useState, useEffect} from 'react';
import { Typography, Button, Form, message, Input, Icon } from 'antd';
import {useSelector, useStore} from "react-redux";
import axios from'axios';

const { Title } = Typography;
const { TextArea } = Input;

const Private = [
    {'value': 0, 'label':'Private'},
    {'value': 1,'label':'Public'}
];
const Catogory = [
    { value: 0, label: "Film & Animation" },
    { value: 0, label: "Autos & Vehicles" },
    { value: 0, label: "Music" },
    { value: 0, label: "Pets & Animals" },
    { value: 0, label: "Sports" },
];

function EditVideoPage(props) {
    const token = localStorage.getItem('token')
    const videoId = props.match.params.videoId
    const user = useSelector(state => state.user);
    const [title, setTitle] = useState("");
    const [Description,setDescription] = useState("");
    const [Privacy,setPrivacy] = useState(0);
    const [Categories,setCategories] = useState("Film & Animation");

    const handleChangeTitle = (event) => {
        
        setTitle(event.currentTarget.value);
    }
    const handleChangeDecsription = (event) => {
        setDescription(event.currentTarget.value);
    }
    const handleChangeOne = (event) => {
        console.log(event.currentTarget.value);
        setPrivacy(event.currentTarget.value);
    }
    const handleChangeTwo = (event) => {
        console.log(event.currentTarget.value);
        setCategories(event.currentTarget.value);
    }
    const onSubmit = (event)=>{
        event.preventDefault();
        if(user.userData && !user.userData.isAuth){
            message.warning("Please log in first")
        }
        if (title === "" || Description === "" ||
            Categories === ""
        ) {
            return message.warning('Please first fill all the fields')
        }
        const variables = {
            videoId: videoId,
            title: title,
            description: Description,
            privacy: Privacy,
            category: Categories,
        }
        axios.post(`${process.env.REACT_APP_SERVER}/api/video/editVideo`, variables,{
            headers:{
                Authorization:'Bearer '+token
            }
        })
        .then(response => {
            if(response.data.success){
                message.success("Edited video success")
                props.history.push('/');
            }else{
                message.error("Failed to edit this video!");
            }
        })
    }
    const videoVariable = {
        videoId: videoId
    }
    useEffect(() => {
            axios.post(`${process.env.REACT_APP_SERVER}/api/video/getVideo`,videoVariable)
            .then(response=>{
                if(response.data.success){
                    console.log(response.data.video)
                    setTitle(response.data.video.title)
                    setDescription(response.data.video.description)
                    setCategories(response.data.video.category)
                    setPrivacy(response.data.video.privacy)
                }else{
                    message.error("Failed to get video infor")
                }
            })
    }, [])
    return (
        <div style={{ maxWidth: '700px', margin: '2rem auto' }}>
        <div style={{ textAlign: 'center', marginBottom: '2rem' }}>
            <Title level={2} > Edit Video </Title>
        </div>

        <Form onSubmit = {onSubmit}>
            <div className = "upload__information">
                <label>Title</label>
                <Input
                    onChange={handleChangeTitle}
                    value={title}
                />
                <br /><br />
                <label>Description</label>
                <TextArea
                    onChange={handleChangeDecsription}
                    value={Description}
                />
                <br /><br />

                <select value = {Privacy} onChange={handleChangeOne}>
                    {Private.map((item, index) => (
                        <option key={index} value={item.value}>{item.label}</option>
                    ))}
                </select>
                <br /><br />

                <select value = {Categories} onChange={handleChangeTwo}>
                    {Catogory.map((item, index) => (
                        <option key={index} value={item.label}>{item.label}</option>
                    ))}
                </select>
                <br /><br />

                <Button className = "upload__button" type="primary" size="large" onClick={onSubmit}>
                    Submit
                </Button>
            </div>
        </Form>
    </div>
    )
}

export default EditVideoPage
