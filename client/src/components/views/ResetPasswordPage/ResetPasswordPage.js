import React,{useState} from "react";
import moment from "moment";
import { Formik } from 'formik';
import * as Yup from 'yup';
import { resetUser } from "../../../_actions/user_actions";
import { useDispatch } from "react-redux";
import axios from "axios";
import {
  Form,
  Input,
  Button,
  Icon
} from 'antd';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

function ResetPasswordPage(props) {
  const dispatch = useDispatch();
  const [sent, setSent] = useState(false);
  const [click, setClick] = useState(false);
  const onHandleSearch = (values)=>{
    setSent(true);
    let dataOTP = {
      email: values.email
    }
    axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/users/send_otp`,dataOTP)
    .then((response)=>{
      if(response.data.error_code == 0){
        setSent(false);
        setClick(true);
      }  
    })
  }
  return (

    <Formik
      initialValues={{
        email: '',
        password: '',
        confirmPassword: '',
        OTP:''
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email('Email is invalid')
          .required('Email is required'),
        password: Yup.string()
          .min(6, 'Password must be at least 6 characters')
          .required('Password is required'),
        confirmPassword: Yup.string()
          .oneOf([Yup.ref('password'), null], 'Passwords must match')
          .required('Confirm Password is required'),
        OTP: Yup.string()
          .required('OTP is required')
      })}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {

          let dataToSubmit = {
            email: values.email,
            password: values.password,
            confirm_password: values.confirmPassword,
            send_otp: 0,
            otp: values.OTP,
          };

          dispatch(resetUser(dataToSubmit)).then(response => {
            if (response.payload.error_code == 0) {
              props.history.push("/login");
            } else {
              alert(response.payload.message)
            }
          })

          setSubmitting(false);
        }, 500);
      }}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          dirty,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
        } = props;
        return (
          <div className="app">
            <h2>Reset Password</h2>
            <Form style={{ minWidth: '375px' }} {...formItemLayout} onSubmit={handleSubmit} >
              <Form.Item required label="Email" hasFeedback validateStatus={errors.email && touched.email ? "error" : 'success'}>
                <Input
                  id="email"
                  placeholder="Enter your Email"
                  type="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.email && touched.email ? 'text-input error' : 'text-input'
                  }
                />
                {errors.email && touched.email && (
                  <div className="input-feedback">{errors.email}</div>
                )}
              </Form.Item>
              <Form.Item required label="OTP" hasFeedback>
                <Input.Search
                  id="OTP"
                  placeholder="Click button to sent OTP"
                  type="text"
                  value={values.OTP}
                  onChange={handleChange}
                  //onBlur={handleBlur}
                  onSearch={()=>onHandleSearch(values)}
                  loading = {sent}
                  enterButton={click?<Icon type="check-circle"/>:<Icon type="bell"/> }
                  className={
                    errors.OTP && touched.OTP ? 'text-input error' : 'text-input'
                  }
                />
                {errors.OTP && touched.OTP && (
                  <div className="input-feedback">{errors.OTP}</div>
                )}
                {click &&(sent ?
                  <div className="input-otp">Sending otp to your email...</div>:
                  <div className="input-otp">OTP has been sent to your email</div>
                )}
              </Form.Item>

              <Form.Item required label="New Password" hasFeedback validateStatus={errors.password && touched.password ? "error" : 'success'}>
                <Input
                  id="password"
                  placeholder="Enter your new password"
                  type="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.password && touched.password ? 'text-input error' : 'text-input'
                  }
                />
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}
              </Form.Item>

              <Form.Item required label="Confirm" hasFeedback>
                <Input
                  id="confirmPassword"
                  placeholder="Re-enter your password"
                  type="password"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.confirmPassword && touched.confirmPassword ? 'text-input error' : 'text-input'
                  }
                />
                {errors.confirmPassword && touched.confirmPassword && (
                  <div className="input-feedback">{errors.confirmPassword}</div>
                )}
              </Form.Item>
              <Form.Item {...tailFormItemLayout}>
                <Button onClick={handleSubmit} type="primary" disabled={isSubmitting}>
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        );
      }}
    </Formik>
  );
};


export default ResetPasswordPage
