import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { loginUser } from "../../../_actions/user_actions";
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Form, Icon, Input, Button, Checkbox, Typography,notification } from 'antd';
import { useDispatch } from "react-redux";
import {client} from "../../Config";

const { Title } = Typography;
window.OneSignal = window.OneSignal || [];
const OneSignal = window.OneSignal;

function LoginPage(props) {
  const dispatch = useDispatch();
  const rememberMeChecked = localStorage.getItem("rememberMe") ? true : false;

  const [formErrorMessage, setFormErrorMessage] = useState('')
  const [rememberMe, setRememberMe] = useState(rememberMeChecked)

  const handleRememberMe = () => {
    setRememberMe(!rememberMe)
  };

  const initialEmail = localStorage.getItem("rememberMe") ? localStorage.getItem("rememberMe") : '';

  return (
    <Formik
      initialValues={{
        email: initialEmail,
        password: '',
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email('Email is invalid')
          .required('Email is required'),
        password: Yup.string()
          .min(6, 'Password must be at least 6 characters')
          .required('Password is required'),
      })}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(()=>{
          let dataToSubmit = {
            email: values.email,
            password: values.password
          };

          dispatch(loginUser(dataToSubmit))
            .then(response => {
              console.log("response login: ",response)
              if (response.payload.error_code === 0) {
                window.localStorage.setItem('token',response.payload.data.token);
                window.localStorage.setItem('userId', response.payload.data.user.id);
                window.localStorage.setItem('user_name', response.payload.data.user.name);
                window.localStorage.setItem('image', response.payload.data.user.image);
                if (rememberMe === true) {
                  window.localStorage.setItem('rememberMe', values.id);
                } else {
                  localStorage.removeItem('rememberMe');
                }
                console.log("Khoa check send tag");
                OneSignal.push(function(){
                  console.log("Send tag one signal");
                  OneSignal.sendTag("user_id", response.payload.data.user.id).then(function(tagSent){
                    console.log("Tag Sent: " + tagSent.user_id);
                  });
                });
                if(client.readyState === 0){
                  client.onopen = () => {
                    console.log('WebSocket Client Connected')
                    client.send(JSON.stringify({
                      command:'open_connection'
                    }))
                    client.onmessage =(message) =>{
                      const dataFromServer = JSON.parse(message.data);
                        if(dataFromServer.type === 'open_connection'){
                          localStorage.setItem("connection_id",dataFromServer.connection_id)
                        }
                        if(dataFromServer.type === 'send-notification'){
                          notification.success({
                              message: "You have a notification!",
                              description:
                                  <span>
                                      {dataFromServer.content}
                                  </span>,
                              placement:'bottomRight',
                          })
                        }
                    }
                  }
                }
                client.send(JSON.stringify({
                  command:'modify_connection',
                  token: response.payload.data.token
                }))
                props.history.push("/");
              } else {
                setFormErrorMessage('Check out your Account or Password again')
              }
            })
            .catch(err => {
              console.log("error:" +err)
              setFormErrorMessage("error:" +err)
              setTimeout(() => {
                setFormErrorMessage("")
              }, 5000);
            });
          setSubmitting(false);
        },500);

      }}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;
        return (
          <div className="app">

            <Title level={2}>Log In</Title>
            <form onSubmit={handleSubmit} style={{ width: '350px' }}>

              <Form.Item required>
                <Input
                  id="email"
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Enter your email"
                  type="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.email && touched.email ? 'text-input error' : 'text-input'
                  }
                />
                {errors.email && touched.email && (
                  <div className="input-feedback">{errors.email}</div>
                )}
              </Form.Item>

              <Form.Item required>
                <Input
                  id="password"
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Enter your password"
                  type="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.password && touched.password ? 'text-input error' : 'text-input'
                  }
                />
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}
              </Form.Item>

              {formErrorMessage && (
                <label ><p style={{ color: '#ff0000bf', fontSize: '0.7rem', border: '1px solid', padding: '1rem', borderRadius: '10px' }}>{formErrorMessage}</p></label>
              )}

              <Form.Item>
                <Checkbox id="rememberMe" onChange={handleRememberMe} checked={rememberMe} >Remember me</Checkbox>
                <a className="login-form-forgot" href="/reset" style={{ float: 'right' }}>
                  forgot password
                  </a>
                <div>
                  <Button type="primary" htmlType="submit" className="login-form-button" style={{ minWidth: '100%' }} disabled={isSubmitting} onSubmit={handleSubmit}>
                    Log in
                </Button>
                </div>
                Or <a href="/register">register now!</a>&nbsp;
                <a style={{float:'right'}} href="/">back to dashboard</a>
              </Form.Item>
            </form>
          </div>
        );
      }}
    </Formik>
  );
};

export default withRouter(LoginPage);


