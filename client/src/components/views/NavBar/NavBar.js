import React, { useState } from 'react';
import LeftMenu from './Sections/LeftMenu';
import RightMenu from './Sections/RightMenu';
import { Drawer, Button, Icon } from 'antd';
import   PlayCircleTwoTone  from '@ant-design/icons/PlayCircleTwoTone';
import './Sections/Navbar.css';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import Search from './Sections/Search';
import Notification from './Sections/Notification';
import {useSelector} from "react-redux";
import { withRouter } from 'react-router-dom';

function NavBar(props) {
  const userId = window.localStorage.getItem('userId')
  const userName = window.localStorage.getItem('user_name')
  const image = window.localStorage.getItem('image')
  const [visible, setVisible] = useState(false)
  const showDrawer = () => {
    setVisible(true)
  };

  const onClose = () => {
    setVisible(false)
  };
  const navigateTab = (url)=>{
    props.history.push(url)
  }    
  return (
    <nav className="menu" style={{ position: 'fixed', zIndex: 5, width: '100%' }}>
      <div className="menu__logo" onClick={()=>navigateTab("/list/all")}>
        <PlayCircleTwoTone />
        <span style={{color:'#1E90FF'}}> Tube Lite</span>
      </div>
      <div className="menu__container">
        <Search />
        {/* <div className="menu_rigth">
          <RightMenu mode="horizontal" />
        </div> */}
        <Button
          className="menu__mobile-button"
          type="primary"
          onClick={showDrawer}
        >
          <Icon type="align-right" />
        </Button>
        <Notification />
        <Drawer
          title={
            userId ? 
              <span>
                <img style={{width:'30px',height:'30px',borderRadius:'10px'}} src={image} />
                &nbsp; {userName}
              </span>
            :"Guest"
          }
          placement="right"
          className="menu_drawer"
          closable={false}
          onClose={onClose}
          visible={visible}
        >
          <RightMenu mode="inline" />
        </Drawer>
      </div>
    </nav>
  )
}

export default withRouter(NavBar)