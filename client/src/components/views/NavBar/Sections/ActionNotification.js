import React from 'react';
import { Menu, Dropdown, Button, Icon, message, Badge } from 'antd';

function ActionNotification(props) {
    function handleActionClick(e){
        console.log('Action Click')
    }
    function handleMenuClick(e) {
      if(e.key == "2"){
        console.log("Delete!")
        props.deleteFunction(props.id,"delete")
      }
      if(e.key == "1"){
        console.log("Mark as read")
        props.deleteFunction(props.id,"markAsRead")
      }
    }
    const menu = (
        <Menu onClick={handleMenuClick}>
          <Menu.Item key="1" >
            Mark as read
          </Menu.Item>
          <Menu.Item key="2" >
            Delete
          </Menu.Item>
        </Menu>
    );
    return (
        <div style={{float:'right',marginLeft:'5px'}}>
            <Dropdown trigger={['hover']} overlay={menu} onClick={handleActionClick}>
            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
            <Icon type="info-circle" />
            </a>
            </Dropdown>
        </div>
    )
}

export default ActionNotification
