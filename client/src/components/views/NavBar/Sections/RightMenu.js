/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Menu, Icon, message} from 'antd';
import axios from 'axios';
import { USER_SERVER, client } from '../../../Config';
import { withRouter } from 'react-router-dom';
 

function RightMenu(props) {
  const userId = localStorage.getItem('userId') 
  const headers = {Authorization:'Bearer '+ window.localStorage.getItem('token')}
  var width = window.innerWidth;
  const logoutHandler = () => {
    axios.get(`${USER_SERVER}/logout`,{headers}).then(response => {
      if (response.status === 200) {
        // client.close();
        window.localStorage.clear();
        props.history.push("/login");
      } else {
        alert('Log Out Failed')
      }
    });
  };
  const UploadHandler = () =>{
    console.log("Clicked upload");
    props.history.push("/video/upload")
  }

  const navigateTab = (url)=>{
    props.history.push(url)
  }
  const handleCheckDevice = (url) =>{
    if(width < 767){
      message.warning("We only support this feature on PC! Please use PC and try again.")
      return;
    }
    props.history.push(url)
  }
  if (!userId) {
    return (
      <Menu mode={props.mode}>
        <Menu.Item key="mail">
          <a href="/login">Signin</a>
        </Menu.Item>
        <Menu.Item key="app">
          <a href="/register">Signup</a>
        </Menu.Item>
      </Menu>
    )
  } else {
    return (
      <Menu mode={props.mode}>
        <Menu.Item key="myChannel">
          <div style={{cursor:'pointer'}} onClick={()=>navigateTab(`/myChannel/${userId}`)}>
            <Icon type="profile" theme="twoTone" />My Channel
          </div>
        </Menu.Item>
        <Menu.SubMenu
          key="sub-livestream"
          title={
            <span>
              <Icon type="fire" theme="twoTone" />Livestream
            </span>
          }
        >
          <Menu.Item key="go-live">
            <div style={{cursor:'pointer'}} onClick={()=>navigateTab(`/livestream/`)}>
              <Icon type="video-camera" theme="twoTone" />Go Live
            </div>
          </Menu.Item>
          <Menu.Item key="list-live">
            <div style={{cursor:'pointer'}} onClick={()=>navigateTab(`/livestream/list/`)}>
              <Icon type="customer-service" theme="twoTone" />List Live
            </div>
          </Menu.Item>
          <Menu.Item key="list-record-live">
            <div style={{cursor:'pointer'}} onClick={()=>navigateTab(`/livestream/list-record/`)}>
              <Icon type="file-zip" theme="twoTone" />List Record Live
            </div>
          </Menu.Item>
        </Menu.SubMenu>
        <Menu.Item key="upload">
          <div style={{cursor:'pointer'}} onClick={()=>UploadHandler()}>
            <Icon type="folder-add" theme="twoTone" />Upload
          </div>
        </Menu.Item>
        <Menu.Item key="statistics">
          <div style={{cursor:'pointer'}} onClick={()=>handleCheckDevice(`/statistics/`)}>
            <Icon type="fund" theme="twoTone" />Statistics
          </div>
        </Menu.Item>
        <Menu.Item key="logout">
          <a onClick={logoutHandler}>
            <Icon type="minus-square" theme="twoTone" />Logout
          </a>
        </Menu.Item>
      </Menu>
    )
  }
}

export default withRouter(RightMenu);

