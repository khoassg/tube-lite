import React,{useState, useEffect, useSelector  } from 'react';
import { Menu, Dropdown, Button, Icon, message, Badge, List, Avatar, notification, Progress } from 'antd';
import ActionNotification from './ActionNotification';
import axios from "axios";
import {client} from "../../../Config"


function Notification() {
    const [count, setCount] = useState(0);
    const [list, setList] = useState([]);
    const userId = localStorage.getItem('userId')
    const token = localStorage.getItem('token')
    const headers = {Authorization:'Bearer '+ token};
    var width = window.innerWidth;
    function handleButtonClick(e) {
        axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/v1/notification/reset-count`,{},
        {
            headers:headers
        }).then((response)=>{
            console.log("response reset count: ",response.data)
            setCount(0)
        })
        axios.get(`${process.env.REACT_APP_PHP_SERVER}/api/v1/notification/list-notification`,{
            headers:{
                Authorization:'Bearer '+token
            }
        }).then((response)=>{
            setList(response.data.data.items)
        })
    }

    function handleMarkAllClick(e) {
        axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/v1/notification/mask-notification-all`,{},
        {
            headers:headers
        }).then((response)=>{
            if(response.data.error_code === 0){
                message.success('Mark all notification as read successfully')
            }else{
                message.error('Failed to mark all notification as read')
            }
        })
    }
        
    function handleMenuClick(e) {
        console.log('click menuItem', e);
    }

    const updateList = (id,action) => {
        if(action == 'delete'){
            axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/v1/notification/delete`,{
                'id': id
            },
            {
                headers:headers
            }).then((response)=>{
                if(response.data.error_code === 0){
                    message.success('Delete notification successfully')
                }else{
                    message.error('Failed to delete notification')
                }
            })
        }
        else {
            console.log("Mark as read id ",id)
            axios.post(`${process.env.REACT_APP_PHP_SERVER}/api/v1/notification/mask-notification`,
            {
                "id":id
            }
            ,{
                headers:headers
            }).then((response)=>{
                if(response.data.error_code === 0){
                    message.success('Mark notification successfully')
                }else{
                    message.error('Failed to mark notification')
                }
            })
        }
    }

    const menuItem = list.map((notification,index)=>{
            return <Menu.Item key={notification.id}>
                        <List.Item className="item-notification">
                            <Badge dot offset={[10,10]} count={notification.is_read === 0 ? 1:0} >
                                <List.Item.Meta
                                    title ={notification.title} 
                                    description={`Created at ${notification.created_at}`}
                                    avatar = {<Avatar src={notification.image}/>}
                                />
                            </Badge>
                            <ActionNotification id={notification.id} deleteFunction ={updateList}/>
                        </List.Item>
                    </Menu.Item>
        })
    const menu = (
        <Menu onClick={handleMenuClick}>
            <Menu.Item disabled={true}>
                <Button type="primary" onClick={handleMarkAllClick}>
                    Mark all as read
                </Button>
            </Menu.Item>
            {
                list.length !== 0 ? menuItem: 
                <Menu.Item disabled={true}>
                    Empty notification
                </Menu.Item>
            }
        </Menu>
    );

    useEffect(() => {
        //Clear previous interval
        if(window.myInterval != undefined && window.myInterval != 'undefined'){
            window.clearInterval(window.myInterval);
            console.log('Timer cleared with id'+window.myInterval);
        }
        if(userId){
            // Call api every 5 seconds
            window.myInterval= setInterval(()=>{
                axios.get(`${process.env.REACT_APP_PHP_SERVER}/api/v1/notification/count-unread-notification`,{
                    headers:{
                        Authorization:'Bearer '+token
                    }
                }).then((response)=>{
                    setCount(response.data.data.unread_count)
                })
            },5000)
        }
    }, [count,userId,client])
    return (
        <div>            
            <Dropdown overlay={menu} trigger= {['click']} 
                placement={width < 767 ? 'bottomRight':'bottomCenter'} 
                arrow
            >
                <Button
                    className="menu__mobile-button__bell"
                    type="primary"
                    onClick = {handleButtonClick}
                >
                    <Badge size="small" offset={[6,-5]} count ={count}>
                    <Icon type="bell" />
                    </Badge>
                </Button>
            </Dropdown>
        </div>
    )
}

export default Notification
