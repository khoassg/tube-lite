import React,{useState,useEffect} from 'react'
import { Redirect } from 'react-router-dom';
import { Input } from 'antd';
import TextField from '@material-ui/core/TextField';
import {Autocomplete} from '@material-ui/lab';
import   SearchOutlined  from '@ant-design/icons/SearchOutlined'
import axios from 'axios'

function Search(props) {
    const [Keyword, setKeyword] = useState('')
    const [redirect, setRedirect] = useState(false)
    const [Options, setOptions] = useState([])
    const handleChange = (e,newInputValue) =>{
        setRedirect(false)
        setKeyword(newInputValue)
    }
    const handleKeyDown = (e) =>{
        if(e.key === 'Enter'){
            if(Keyword === ''){
                setKeyword('all')
            }
            setRedirect(true)
        }
    }
    useEffect(() => {
        axios.get(`${process.env.REACT_APP_SERVER}/api/video/getTitle`)
        .then(response=>{
            if(response.data.success){
                setOptions(response.data.titles)
            }else{
                alert("Failed to get options search!")
            }
        })
    }, [])
    return (
        <div className='search'>
            {/* <Input 
                placeholder='Search some thing...' 
                value={Keyword} 
                onChange={handleChange} 
                onKeyDown={handleKeyDown}
                className = 'auto_complete'
            /> */}
            <div className="wrapper">
                <Autocomplete
                    id="combo-box-demo"
                    options={Options}
                    inputValue={Keyword}
                    onInputChange={handleChange}
                    onKeyDown = {handleKeyDown}
                    getOptionLabel={(option) => option}
                    className = 'auto_complete'
                    renderInput={(params) => <TextField {...params}/>}
                />
                <button >
                    <a href={`/list/${Keyword === ''? 'all':Keyword}`}>
                        <SearchOutlined />
                    </a>
                </button>
            </div>
            {redirect ? (<Redirect push to ={`/list/${Keyword}`}/>):null}
        </div>
    )
}

export default Search
