import React, { Suspense,useState,useEffect } from 'react';
import { Route, Switch } from "react-router-dom";
import Auth from "../hoc/auth";
// pages for this product
import LandingPage from "./views/LandingPage/LandingPage.js";
import LoginPage from "./views/LoginPage/LoginPage.js";
import RegisterPage from "./views/RegisterPage/RegisterPage.js";
import NavBar from "./views/NavBar/NavBar";
import Footer from "./views/Footer/Footer";
import UploadVideoPage from "./views/UploadVideoPage/UploadVideoPage";
import DetailVideoPage from "./views/DetailVideoPage/DetailVideoPage";
import EditVideoPage from "./views/EditVideoPage/EditVideoPage";
import Dashboard from "./views/LandingPage/Dashboard";
import MyChannelPage from "./views/MyChannelPage/MyChannelPage";
import StatisticsPage from "./views/StatisticsPage/StatisticsPage";
import Livestream from "./views/Livestream/Livestream";
import ResetPasswordPage from "./views/ResetPasswordPage/ResetPasswordPage";
import {client} from "../components/Config";
import VideoLivestream from './views/Livestream/VideoLivestream';
import ListLivestream from './views/Livestream/ListLivestream';
import ListRecordLivestream from './views/Livestream/ListRecordLivestream';
import {notification} from 'antd';
//null   Anyone Can go inside
//true   only logged in user can go inside
//false  logged in user can't go inside

// const client = new w3cwebsocket('ws://127.0.0.1:3001');

function App() {
  const token = localStorage.getItem('token');
  useEffect(() => {
    if(client.readyState === 0){
      client.onopen = () => {
        console.log('WebSocket Client Connected')
        client.send(JSON.stringify({
          command:'open_connection'
        }))
        client.onmessage =(message) =>{
          const dataFromServer = JSON.parse(message.data);
            if(dataFromServer.type === 'open_connection'){
              localStorage.setItem("connection_id",dataFromServer.connection_id)
            }
            if(dataFromServer.type === 'send-notification'){
              notification.success({
                  message: "You have a notification!",
                  description:
                      <span>
                          {dataFromServer.content}
                      </span>,
                  placement:'bottomRight',
              })
            }
        }
        if(token){
          client.send(JSON.stringify({
            command:'modify_connection',
            token: token
          }))
        }
      }
    }
  }, [token])
  return (
    <Suspense fallback={(<div>Loading...</div>)}>
      <Switch>
        <Route exact path="/login" component={Auth(LoginPage, false)} />
        <Route exact path="/register" component={Auth(RegisterPage, false)} />
        <Route exact path="/reset" component={Auth(ResetPasswordPage, false)} />
      </Switch>
      <NavBar/>
      <div style={{ paddingTop: '69px', minHeight: 'calc(100vh - 80px)' }}>
        <Switch>
          <Route exact path ="/" component={Auth(Dashboard,null)}/>
          <Route exact path="/list/:keyword" component={Auth(LandingPage, null)} />
          <Route exact path="/video/upload" component={Auth(UploadVideoPage, true)} />
          <Route exact path="/video/:videoId" component={Auth(DetailVideoPage, null)} />
          <Route exact path="/edit/:videoId" component={Auth(EditVideoPage, true)} />
          <Route exact path="/myChannel/:userId" component={Auth(MyChannelPage, true)} />
          <Route exact path="/statistics/" component={Auth(StatisticsPage, true)} />
          <Route exact path="/livestream/" component={Auth(Livestream, true)} />
          <Route exact path="/livestream/video/:userId/:roomId" component={Auth(VideoLivestream, true)} />
          <Route exact path="/livestream/list/" component={Auth(ListLivestream, true)} />
          <Route exact path="/livestream/list-record/" component={Auth(ListRecordLivestream, true)} />
        </Switch>
      </div>
      <Footer />
    </Suspense>
  );
}

export default App;
