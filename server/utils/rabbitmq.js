var amqp = require('amqplib/callback_api');
const webSocket = require("../routes/websocket")
const { Notification } = require("../models/Notification");
async function sendToSocket(msg){
    var noti = new Notification(msg);
    noti.save();
    var response = JSON.stringify(msg);
    var users = msg.users;
    users.forEach((user) => {
        webSocket.sendToUser(user,response);
    })   
}
function sendToRabbit(queue_name,msg){
    return sendToSocket(msg);
    var user = process.env.RABBITMQ_QUEUE_USER
    var password = process.env.RABBITMQ_QUEUE_PASSWORD
    var port = process.env.RABBITMQ_PORT
    var host = process.env.RABBITMQ_DOMAIN
    var vhost = process.env.RABBITMQ_VHOST
    var uri = process.env.RABBITMQ_URI;
    console.log(uri);
    amqp.connect(uri, function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
                throw error1;
            }

            var publish_exchange = queue_name + '.exchange';
            channel.assertExchange(publish_exchange, 'direct', {
                durable: false,
                autoDelete:true,
            });

            channel.publish(publish_exchange, '',Buffer.from(JSON.stringify(msg)));
        });
        setTimeout(function(){
            connection.close();
        },500)
    });
}

module.exports = {
    sendToRabbit
};