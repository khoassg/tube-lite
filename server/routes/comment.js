const express = require('express');
const router = express.Router();
const { Comment } = require("../models/Comment");
const { Video } = require("../models/Video");
const mongoose = require("mongoose");
const { auth } = require("../middleware/auth");
const rabbitmq = require("../utils/rabbitmq");
const websocket = require("../routes/websocket");
var moment = require('moment');
//=================================
//             Comment
//=================================
router.use("/saveComment", auth)
router.post("/saveComment", (req, res) => {
    comment = new Comment(req.body)
    var user = req.user
    if (comment.roomId) {
        roomId = comment.roomId        
        ws = socketByUserId[user._id.toString()]
        var data = {
            writer:{
                name: user.name,
                image: user.image,
            },
            content: comment.content,
            type: "chat",
            roomId: roomId,
            createdAt: moment().format()
        }
        websocket.broadCastInRoom(ws,roomId,JSON.stringify(data))
    }
    else {
        //Tìm video
        Video.findOne({
            _id: mongoose.Types.ObjectId(req.body.postId)
        }, (err, video) => {
            if (err) return res.json({ success: false, err })
            if (!video) {
                return res.json({ success: false, message: "Video not found" })
            }
            var msg = {
                users: [video.writer.toString()],
                content: `${user.name} has just commented on our video`,
                type: "send-notification",
                created_user_id: user._id,
                image:user.image,
            }
            rabbitmq.sendToRabbit('notification', msg)
        })
    }
    comment.save((err, comment) => {
        if (err) return res.json({ success: false, err })
        // if(req.body.responseTo){
        //
        // }
        Comment.findOne({ '_id': comment._id })
            .populate('writer')
            .exec((err, result) => {
                if (err) return res.json({ success: false, err })
                res.json({ success: true, comment: result })
            })
    })
});
router.post("/getComments", (req, res) => {
    if(req.body.videoId){
        Comment.find({ 'postId': req.body.videoId })
        .populate('writer')
        .exec((err, comments) => {
            if (err) return res.status(400).send(err)
            res.status(200).json({ success: true, comments })
        })
    }else{
        Comment.find({ 'roomId': req.body.roomId })
        .populate('writer')
        .exec((err, comments) => {
            if (err) return res.status(400).send(err)
            res.status(200).json({ success: true, comments })
        })
    }
})


module.exports = router;
