const express = require('express');
const router = express.Router();
const { Like } = require("../models/Like");
const { auth } = require("../middleware/auth");
const rabbitmq = require("../utils/rabbitmq");
//=================================
//             Subscribe
//=================================

router.post("/getLikes",(req,res)=>{
    let likes = []
    let dislikes = []
    let variable = {}
    if(req.body.videoId){
        variable = {"videoId":req.body.videoId}
    }else{
        variable = {"commentId":req.body.commentId}
    }
    Like.find(variable).exec((err,lists)=>{
        if(err) return res.status(400).json({success:fales})
        console.log("get Like:",lists)
        lists.map(object =>{
            if(object.type === 'like'){
                likes.push(object)
            }else if(object.type === 'dislike'){
                dislikes.push(object)
            }
        })
        res.status(200).json({success:true,likes:likes,dislikes:dislikes})
    })
});
router.use('/uplike',auth);
router.use('/unlike',auth);
router.post("/uplike",(req,res)=>{
    var user = req.user;
    let variable = {userId:req.body.userId,type:req.body.type}
    var object_string;
    if(req.body.videoId){
        variable['videoId'] = req.body.videoId
        object_string = 'video';
    }else{
       variable['commentId'] = req.body.commentId;
        object_string = 'bình luận';
    }
    variable['owner_id'] = req.body.owner_id;
    const like = new Like(variable)
    console.log('up like:',like)
    like.save((err,likeResult)=>{
        if(err) return res.status(400).json({success:false,err})
        var content
        if(req.body.type === 'dislike'){
            variable['type'] = 'like'
            content =  `${user.name} has just disliked your ${object_string}`;
        }else if(req.body.type === 'like'){
            variable['type'] = 'dislike'
            content =  `${user.name} has just liked your ${object_string}`;
        }
        Like.findOneAndDelete(variable)
        .exec((err,dislikeResult)=>{
            if(err) return res.status(400).json({success:false,err})
            res.status(200).json({success:true})
            var msg = {
                users: [req.body.owner_id],
                content,
                created_user_id: user._id,
                type: "send-notification",
                image:user.image,
            }
            rabbitmq.sendToRabbit('notification',msg)
        })
    })
})

router.post("/unlike",(req,res)=>{
    let variable = {userId:req.body.userId,type:req.body.type}
    if(req.body.videoId){
        variable['videoId'] = req.body.videoId
    }else{
       variable['commentId'] = req.body.commentId
    }
    Like.findOneAndDelete(variable)
    .exec((err,result)=>{
        if(err) return res.status(400).json({success:false,err})
        res.status(200).json({success:true})
    })
})

module.exports = router;
