const WebSocket = require('ws');
const { v4: uuidv4 } = require('uuid');
const helper = require('../utils/helper');
const jwt = require('jsonwebtoken');
const { Comment } = require("../models/Comment");

global.socketManager = {};
global.roomManager = {};
global.roomByConnectionId = {};
global.socketByUserId = {}; 
const wss = new WebSocket.Server({
    port: 3001
})
function heartbeat() {
    this.isAlive = true;
}
function noop() {}
function broadCast(ws,data){
    wss.clients.forEach((client) => {
        if(client !== ws && client.readyState === WebSocket.OPEN){
            client.send(data)
        }
    })
}
function validateOpenConnection(ws){
    let connection_id = helper.getKeyByValue(socketManager,ws)    
    if(!connection_id){
        ws.send('Not open connection yet')
        ws.terminate();
    }
}
function validateRoomCreated(ws,room_id){
    if(!roomManager[room_id]){
        message = 'Room has not been created';
        ws.send(JSON.stringify({message}))        
        return false;
    }
    return true;
}
function broadCastInRoom(ws,room_id,data){
    var list_sockets = roomManager[room_id]
    if(list_sockets){
        list_sockets.forEach((client) => {
            if(client != ws && client.readyState === WebSocket.OPEN){
                client.send(data)
            }
        })
    }
}
function sendToUser(user_id,data){
   var ws = socketByUserId[user_id];
   console.log('user id '+ user_id);   
   console.log('socketByUserIdsssss ' + socketByUserId);
   console.log('ws '+ ws);   
   if(ws){
       ws.send(data);
   }
}
function joinRoom(ws,room_id,name){
    let connection_id = helper.getKeyByValue(socketManager,ws)
    let viewers = 0;
    if(roomManager[room_id]){
        //User chưa có trong room thì mới push, còn rồi thì không push
        if(roomManager[room_id].includes(ws)){
            let response = {
                message: `You have joined this room before`,                
            }
            ws.send(JSON.stringify(response))            
            return;
        }
        roomManager[room_id].push(ws)    
    }
    else{
        roomManager[room_id] = [ws];
    }                

    if(roomByConnectionId[connection_id]){
        roomByConnectionId[connection_id].push(room_id);
    }
    else{
        roomByConnectionId[connection_id] = [room_id];
    }
    viewers = roomManager[room_id].length;    
    let response = {
        type:'server_join_room',
        message: `${name} has just joined the room`,
        total: viewers
    }
    var data = {
        type:'notice-room',
        content: `${name} has just joined the room`,        
        roomId: room_id,        
    }
    var comment = new Comment(data)
    comment.save()
    broadCastInRoom(ws,room_id,JSON.stringify(response));
}
function leftRoom(ws,room_id,name){
    let connection_id = helper.getKeyByValue(socketManager,ws)
    let viewers = 0;
    var list_joined_sockets = roomManager[room_id];
    if(list_joined_sockets){    
        for(let i =0; i < list_joined_sockets.length; i++){
            if(list_joined_sockets[i] == ws){
                list_joined_sockets.splice(i,1);
            }
        }                
        roomManager[room_id] = list_joined_sockets;
        viewers = list_joined_sockets.length;
    }    

    var list_rooms_by_connection_id = roomByConnectionId[connection_id];
    if(list_rooms_by_connection_id){
        for(let i =0; i < list_rooms_by_connection_id.length; i++){
            if(list_rooms_by_connection_id[i] == room_id){
                list_rooms_by_connection_id.splice(i,1);
            }
        }                
        roomByConnectionId[connection_id] = list_rooms_by_connection_id;
    }
    let response = {
        type:'server_left_room',
        message: `${name} has just left the room`,
        total: viewers
    }
    var data = {
        type:'notice-room',
        content: `${name} has just left the room`,
        roomId: room_id,        
    }
    var comment = new Comment(data)
    comment.save()
    broadCastInRoom(ws,room_id,JSON.stringify(response));
}
wss.on('connection',function connection(ws,request){
    ws.isAlive = true;
    ws.on('pong', heartbeat);
    ws.on('message',function incoming(data){
        let data_obj = JSON.parse(data)    
        // try{
        //     let data_obj = JSON.parse(data)    
        // }
        // catch(err){
        //     console.log(err);
        //     return;
        //     ws.send('wrong data')    
        // }        
        switch (data_obj.command){
            case 'open_connection':{
                let connection_id = helper.getKeyByValue(socketManager,ws);
                if(connection_id){
                    ws.send(JSON.stringify({message: 'open connection yet'}));        
                    return;
                }
                connection_id = uuidv4().toString();
                socketManager[connection_id] = ws;
                ws.send(JSON.stringify({
                    "type":"open_connection",
                    "connection_id":connection_id
                }));
                break;
            }
            case 'modify_connection':{
                let connection_id = helper.getKeyByValue(socketManager,ws);
                if(connection_id){
                    var token = jwt.decode(data_obj.token);
                    socketByUserId[token.sub] = ws;
                    console.log('sub ' + token.sub);
                    console.log('socketByUserId ' + socketByUserId);
                    ws.send(JSON.stringify({message:token.sub}));
                }
            }
            case 'join_room':{
                validateOpenConnection(ws);
                let room_id = data_obj.room_id;
                let connnection_id = data_obj.connection_id;
                let name = data_obj.name;
                console.log("join room with name: ",name)
                joinRoom(ws,room_id,name);                          
                break;
            }
            case 'left_room':{
                validateOpenConnection(ws);
                let room_id = data_obj.room_id;  
                let name = data_obj.name;              
                console.log("left room with name: ",name)
                leftRoom(ws,room_id,name);
                break;
            }
            //Hiện tại chỉ cho chat trong room
            case 'send_to':{
                validateOpenConnection(ws);
                let object_id = data_obj.object_id;
                let type = data_obj.type; //type: room hoặc user
                switch(type){
                    case 'room':{                               
                        let message;
                        validateRoomCreated(ws,object_id);                        
                        if(!roomManager[object_id].includes(ws)){
                            message = 'You have not joined in this room';
                            ws.send(JSON.stringify({message}))
                            return;
                        }                                                  
                        let response = {
                            message: data_obj.message,
                            user_name: data_obj.name
                        }                 
                        broadCastInRoom(ws,object_id,JSON.stringify(response));
                        //Lưu xuống db

                        break;
                    }
                    default: break;
                }                                
            }
            default:{
                break;
            }
        }

    })
    ws.on('close', function close() {
        removeConnection(ws)
        clearInterval(interval);
    });
})

const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
        if (ws.isAlive === false){        
            removeConnection(ws)
            return ws.terminate();
        }
        ws.isAlive = false;
        ws.ping(noop);
    });
}, 5000);

function removeConnection(ws){
    let connection_id = helper.getKeyByValue(socketManager,ws)
    if(connection_id){
        //Thoát hết các room mà ws đó đã join
        let list_rooms = roomByConnectionId[connection_id]
        if(list_rooms){
            list_rooms.forEach((room_id) => {
                leftRoom(ws,room_id)
            })
        }
        //Xóa room id theo connection
        delete roomByConnectionId[connection_id];
        //Xóa connection
        delete socketManager[connection_id];        
    }
}
module.exports = {
    sendToUser,
    broadCastInRoom
};