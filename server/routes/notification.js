const express = require('express');
const { send } = require('process');
const router = express.Router();


router.get("/sendNotification",(req1,res1)=>{
    var sendNotification = function(data) {
        var headers = {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": "Basic YWEwZTEzNmYtNWExNi00MGUwLWFlOTQtZTRkYzk4OGNlZTky"
        };
        
        var options = {
          host: "onesignal.com",
          port: 443,
          path: "/api/v1/notifications",
          method: "POST",
          headers: headers
        };
        
        var https = require('https');
        var req = https.request(options, function(res) {  
          res.on('data', function(data) {
            return res1.status(200).send({"Message":data});
          });
        });
        
        req.on('error', function(e) {
          return res1.status(500).send({"Error": e});
        });
        
        req.write(JSON.stringify(data));
        req.end();
      };
      
    var message = { 
        app_id: "3e8a81fb-c43f-40e3-a0d2-1990834a4e26",
        contents: {"en": "Thank you for subscribing!"},
        headings :{"en":"Khoa check notification"},
        included_segments: ["Active Users"]
    };
    
    sendNotification(message);
});

module.exports = router;