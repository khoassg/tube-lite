const express = require('express');
const router = express.Router();
const { User } = require("../models/User");
const { Subscriber } = require("../models/Subscriber");
const rabbitmq = require("../utils/rabbitmq");
const { auth } = require("../middleware/auth");
var moment = require('moment');
const { Video } = require('../models/Video');

router.get('/list', (req, res) => {
    if (req.query.user_id) {
        User.findById(req.query.user_id, (err, user) => {
            var obj = {
                title: user.livestream_title,
                livestream_key: user.livestream_key,
                livestream_room: user.livestream_room,
                writer: user,
                url: `${process.env.STREAMING_URL}/hls/${user.live_stream_key}.m3u8`
            }
            return res.status(200).json({
                success: true,
                data: obj
            }).send()
        })
    } else {
        User.find({
            live_on: 1
        }).exec((err, users) => {
            if (err) return res.json({ success: false, err })
            var data = [];
            if (users) {
                users.forEach((user) => {
                    var obj = {
                        title: user.livestream_title,
                        livestream_key: user.livestream_key,
                        livestream_room: user.livestream_room,
                        writer: user,
                        url: `${process.env.STREAMING_URL}/hls/${user.live_stream_key}.m3u8`
                    }
                    data.push(obj)
                })
            }
            return res.status(200).json({
                success: true,
                videos: data
            })
        })
    }
})
router.get('/check-live-stream', (req, res) => {
    User.findById(req.query.user_id, (err, user) => {
        if (user.live_on) {
            return res.status(200).json({
                success: true,
                live: 1
            })
        } else {
            return res.status(200).json({
                success: true,
                live: 0
            })
        }
    })
})
router.get('/getVideo', (req, res) => {
    console.log("_id: ", req.body.userId)
    User.findOne({
        "_id": req.query.userId
    }, (err, user) => {
        if (err) {
            return res.status(400).json({
                success: false,
                err
            })
        }
        console.log("Khoa check description: ", user.livestream_description)
        return res.status(200).json({
            success: true,
            user: {
                livestream_title: user.livestream_title,
                livestream_description: user.livestream_description,
                url: `${process.env.STREAMING_URL}/hls/${user.livestream_key}.m3u8`,
                livestream_room: user.livestream_room,
                image: user.image,
                name: user.name,
                livestream_createdAt: user.livestream_createdAt
            }
        });
    })
})
router.use('/update-stream-setting', auth);
router.post('/update-stream-setting', (req, res) => {
    var user = req.user;
    const roomId = Math.floor(Math.random() * 100000);
    User.updateOne({ "_id": user._id }, {
        $set: {
            livestream_title: req.body.title,
            livestream_description: req.body.description,
            live_on: Number(req.body.live_on),
            livestream_room: roomId,
            livestream_createdAt: moment().format()
        }
    }).exec((err, user) => {
        if (err) return res.json({ success: false, err })
        var sub_users = []
        console.log('where userssssssssssssssssssssssssssss')
        Subscriber.find({ "userTo": req.user._id }).exec((err, subscribe) => {            
            console.log('zzzzzzzzzzzzzzzzzzzz')
            console.log(user._id)
            console.log(subscribe)
            if (subscribe.length > 0) {
                subscribe.forEach((user) => {
                    sub_users.push(user.userFrom.toString())
                })
                console.log((sub_users))
                if (sub_users.length > 0) {
                    var msg = {
                        users: sub_users,
                        content: `${req.user.name} has livestream`,
                        type: "send-notification",
                        created_user_id: req.user._id,
                        image: req.user.image,
                    }
                    rabbitmq.sendToRabbit('notification', msg)
                }
            }
        })
        return res.status(200).json({
            success: true,
            roomId: roomId
        })
    })
})
router.post('/off-stream', (req, res) => {
    var streamkey = req.body.name
    var filePath = req.body.path
    var time = moment().format('YYYY-MM-DD H:mm:s');

    User.findOne({
        livestream_key: streamkey,
        deleted_at: null,
    }, (err, user) => {
        if (err) {
            console.log(err)
        }
        user.live_on = 0
        user.save(() => {
            console.log("Save user successfully!")
        })
        if (filePath) {
            filePath = filePath.replace('/var/rec/', 'uploads/') // Biến path thành path product
            filePath = filePath.replace('.flv', '.mp4') // đổi ext
            console.log(filePath)
            var video = new Video({
                writer: user._id,
                title: `${user.livestream_title}`,
                description: `${user.livestream_description}`,
                category: 'Livestream',
                duration: `${time}`,
                thumbnail: user.image,
                filePath: filePath,
                privacy: 1
            })
            video.save((err, video) => {
                if (err) return res.status(400).json({ success: false, err })
            })
        }
        return res.status(200).json({
            success: true
        })
    })
})
router.post('/record-done', (req, res) => {
    var streamkey = req.body.name
    var filePath = req.body.path
    console.log('zzzz');
    console.log(req.body)
    return res.status(200).send({
        success: true
    });
    User.findOne({
        livestream_key: streamkey,
        deleted_at: null,
    }, (err, user) => {
        if (err) {
            console.log(err)
        }
        user.live_on = 0
        user.save(() => {
            //console.log(user)
            return res.status(200).send({
                success: true
            });
        })
    })
})
router.post('/auth', (req, res) => {
    var streamkey = req.body.name
    User.findOne({
        livestream_key: streamkey,
        deleted_at: null,
    }, (err, user) => {
        if (err) {
            console.log(err)
        }
        if (!user) {
            return res.status(403).send();
        }
        return res.status(200).send();
    })
})
router.get('/room-viewers', (req, res) => {
    var roomId = req.query.roomId
    console.log(roomId)
    viewers = 0
    if (roomId) {
        console.log(global.roomManager)
        if (global.roomManager[roomId]) {
            viewers = roomManager[roomId].length;
        }
    }
    return res.status(200).send({
        success: true,
        viewers
    });
})
module.exports = router;
