const express = require('express');
const router = express.Router();
const { Subscriber } = require("../models/Subscriber");
const { User } = require("../models/User.js");
const rabbitmq = require("../utils/rabbitmq");
const { auth } = require("../middleware/auth");
const mongoose = require("mongoose");
//=================================
//             Subscribe
//=================================
router.use('/subscribe',auth);
router.use('/unSubscribe',auth);
router.post("/subscribeNumber",(req,res)=>{
    Subscriber.find({"userTo":req.body.userTo})
    .exec((err,subscribe) =>{
        if(err) return res.status(400).send(err)
        res.status(200).json({ success: true,subscribeNumber:subscribe.length })
    })
});

router.post("/subscribed",(req,res)=>{
    Subscriber.find({"userTo":req.body.userTo,"userFrom":req.body.userFrom})
    .exec((err,subscribe) =>{
        let result = false;
        if(subscribe.length !== 0){
            result = true;
        }
        if(err) return res.status(400).send(err)
        res.status(200).json({ success: true,subscribed:result })
    })
});

router.post("/subscribe",(req,res)=>{
    const subscriber = new Subscriber(req.body)
    var user = req.user;
    subscriber.save((err,data) => {
        if(err) return res.status(400).send(err)
        res.status(200).json({success:true})
        var msg = {
            users: [req.body.userTo],
            content: `${user.name} has just followed your channel`,
            type: "send-notification",
            created_user_id: user._id,
            image:user.image,
        }
        console.log(msg);
        rabbitmq.sendToRabbit('notification',msg)
    })
});

router.post("/unSubscribe",(req,res)=>{
    var user = req.user;
    Subscriber.findOneAndDelete({"userTo": req.body.userTo, "userFrom": req.body.userFrom})
        .exec((err, data) => {
            if (err) return res.status(400).send(err)
            res.status(200).json({success: true})
            var msg = {
                users: [req.body.userTo],
                content: `${user.name} has just unfollowed your channel`,
                type: "send-notification",
                created_user_id: user._id,
                image:user.image,
            }
            console.log(msg);
            rabbitmq.sendToRabbit('notification', msg)
        })
});

module.exports = router;
