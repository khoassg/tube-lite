const express = require('express');
const router = express.Router();
const { Video } = require("../models/Video");
const { Like } = require("../models/Like");
const { User } = require("../models/User");
const multer = require('multer');
const { auth } = require("../middleware/auth");
const { exec } = require('child_process')
const {sendToUser} = require('./websocket');
var ffmpeg = require("fluent-ffmpeg");
const uploads = multer();
var fs = require('fs');
function stringToSlug(str) {
    // remove accents
    var from = "àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ",
        to   = "aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy";
    for (var i=0, l=from.length ; i < l ; i++) {
      str = str.replace(RegExp(from[i], "gi"), to[i]);
    }
  
    str = str
          .trim()
          .replace(/-+/g, '-');
  
    return str.toLowerCase();
}

var storage = multer.diskStorage({
    destination: function(req,file,cb){
        cb(null,'uploads/');
    },
    filename:function (req,file,cb){
        console.log(req.files[0].buffer)
        if(req.body.part){
            cb(null,`${req.body.part}_${req.body.fileName}`);
        }
        else{
            cb(null,`${req.body.fileName}_${req.body.part}`);
        }
    },
    fileFilter: (req,file,cb) => {
        const ext = path.extname(file.originalname);
        if(ext !== '.mp4'){
            return cb(res.status(400).end('Only mp4 file is allowed'),false);
        }
        cb(null,true);
    }
});

var upload = multer({storage:storage}).single("file");

async function joinVideo(total_parts,fileName,filePath,user_id){
    for(let i =1;i<=total_parts;i++){
        var data =fs.readFileSync(`uploads/${i}_${fileName}`)
        fs.appendFileSync(filePath,data)
        fs.unlinkSync(`uploads/${i}_${fileName}`);
    }
    let message = {
        type: 'upload_video',
        upload_completed:1,
        filePath: filePath
    }
    console.log('user _id b4 ' +user_id);
    sendToUser(user_id,JSON.stringify(message))
}
router.use('/uploadfiles',auth);
router.post("/uploadfiles", uploads.any(),(req, res) => {
    fs.writeFile(`uploads/${req.body.part}_${req.body.fileName}`,req.files[0].buffer,(err)=>{
        if(Number(req.body.is_final) === 1){
            var fileName = `${req.body.fileName}`;
            var filePath = `uploads/${fileName}`
            console.log(filePath);
            var total_parts = req.body.total_parts;
            var user_id = req.body.user_id;
            joinVideo(total_parts,fileName,filePath,user_id);
            res.json({success:true,filePath,fileName});
        }
        else{
            res.json({success:true});
        }
        return res;
    })
});

router.post("/uploadVideo", (req, res) => {
    const video = new Video(req.body)
    video.save((err, video)=>{
        if(err) return res.status(400).json({success:false,err})
        return res.status(200).json({
            success:true,
            path: video.filePath
        })
    })
});
router.get('/test',(req, res) => {
    for(let i = 1; i<=3;i++){
        console.log(i)
        var data =fs.readFileSync(`uploads/${i}_test.mp4`)
            // console.log(data)
            // fs.appendFile('uploads/testok.mp4',data,function(){
            //
            // })
        console.log(data)
        fs.appendFile('uploads/testok.mp4',data,function(){

        })
    }

    return res

})
router.get("/getVideos", (req, res) => {
    category = req.query.type;
    if(category){
        Video.find({"privacy":1,"category":category}).populate("writer")
        .exec((err,videos)=>{
            if(err) return res.status(400).send(err)
            res.status(200).json({success:true,videos})
        })
    }else{
        Video.find({"privacy":1,"category": { '$ne':"Livestream" } }).populate("writer")
        .exec((err,videos)=>{
            if(err) return res.status(400).send(err)
            res.status(200).json({success:true,videos})
        })   
    }
});

router.post("/search",(req,res) =>{
    Video.find({'title': new RegExp(req.body.title)}).populate("writer")
    .exec((err,videos)=>{
        if(err) return res.status(400).json({sucees:false,err})
        res.status(200).json({success:true,videos})
    })
})

router.get("/getTitle",(req,res) =>{
    let title_array = []
    Video.find({privacy:1},{title:true})
    .exec((err,titles)=>{
        if(err) return res.status(400).json({sucees:false,err})
        titles.map((title)=>(
            title_array.push(title.title)
        ))
        res.status(200).json({success:true,titles:title_array})
    })
})

router.post("/getVideo", (req, res) => {
    Video.findOne({"_id":req.body.videoId}).populate("writer")
    .exec((err,video)=>{
        if(err) return res.status(400).send(err);
        res.status(200).json({success:true,video})
    })
});

router.post("/editVideo", (req, res) => {
    var newValue = { $set : req.body}
    Video.updateOne({"_id":req.body.videoId},newValue).exec((err,video)=>{
        if(err) return res.status(400).json({success:false,err})
        return res.status(200).json({
            success:true
        })
    })
});

router.post("/deleteVideo", (req, res) => {
    console.log(req.body.videoId)
    Video.findOne({"_id":req.body.videoId})
    .exec((err,video)=>{
        if(err){
            throw err;
        }
        if(video){
            fs.unlink(video.thumbnail,(err) =>{
                if(err){
                    throw err;
                }
                console.log('Thumbnail is deleted')
            })
            fs.unlink(video.filePath,(err) =>{
                if(err){
                    throw err;
                }
                console.log('Video is deleted')
            })
        }
    })
    Video.deleteOne({"_id":req.body.videoId}).exec((err,video)=>{
        if(err) return res.status(400).json({success:false,err})
        return res.status(200).json({
            success:true
        })
    })
});

router.get("/getChannel", (req, res) => {
    Video.find({"writer":req.query.writer}).populate("writer")
    .exec((err,videos)=>{
        if(err) return res.status(400).send(err)
        res.status(200).json({success:true,videos})
    })
});

router.post("/clickView",(req,res)=>{
    let views = 0;
    let variable = {'videoId':req.body.videoId,'type':'view'}
    if(req.body.userId){
        variable['userId'] = req.body.userId
    }
    variable['owner_id'] = req.body.owner_id;
    Video.findOne({"_id":req.body.videoId})
    .exec((err,video)=>{
        if(err) return res.status(400).send(err);
        views = video.views+1;
        const like = new Like(variable)
        like.save();
        var newValue = { $set : {views}}
        Video.updateOne({"_id":req.body.videoId},newValue).exec((err,video)=>{
            if(err) return res.status(400).json({success:false,err})
            return res.status(200).json({
                success:true
            })
        })
    })
})

router.post("/thumbnail", (req, res) => {
    let thumbsFilePath = "";
    let fileDuration = "";
    console.log(req.body.filePath);
    ffmpeg.ffprobe(req.body.filePath, function(err,metadata){
        console.dir(metadata);
        console.log('errr' + err)
        console.log(metadata);
        //console.log(metadata.format.duration);
        fileDuration = metadata.format.duration;
    })
    ffmpeg(req.body.filePath)
        .on('filenames',function(filenames){
            console.log('Will generate' + filenames.join(', '));
            thumbsFilePath = 'uploads/thumbnails/' + filenames[0];
            console.log(thumbsFilePath);
        })
        .on('end',function(){
            console.log('Screenshots taken');
            return res.json({success: true,thumbsFilePath:thumbsFilePath,fileDuration:fileDuration});
        })
        .screenshot({
            //Will take screen at 25%, 50%, 75% of the video 
            count: 1,
            folder:'uploads/thumbnails',
            size: '320x240',
            filename:'thumbnail-%b.png'
        })

});
router.get('/list-livestream',(req,res) => {
    User.find({
        live_on:1,
        deleted_at:null,
    },(users) => {
        var data = [];
        users.forEach((user) => {
            var obj = {
                live_stream_key: user.live_stream_key,
                title: user.live_stream_title,
                
            }
        })
    })
})
module.exports = router;
