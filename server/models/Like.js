const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const likeSchema = mongoose.Schema({
    userId:{
        type: Schema.Types.ObjectId,
        ref:'User'
    },
    commentId:{
        type:Schema.Types.ObjectId,
        ref:'Comment'
    },
    videoId:{
        type:Schema.Types.ObjectId,
        ref:'Video'
    },
    type:{
        type:String
    },
    //Id của thằng đăng video hay đăng comment
    owner_id: {
        type:Schema.Types.ObjectId,
        ref: 'User'
    },
},{timestamps: true})



const Like = mongoose.model('like', likeSchema);

module.exports = { Like }