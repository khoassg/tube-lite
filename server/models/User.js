const mongoose = require('mongoose');


const userSchema = mongoose.Schema({
    name: {
        type:String,
        maxlength:50
    },
    email: {
        type:String,
        trim:true,
        unique: 1 
    },
    password: {
        type: String,
        minglength: 5
    },
    lastname: {
        type:String,
        maxlength: 50
    },
    role : {
        type:Number,
        default: 0 
    },
    image: String,
    token : {
        type: String,
    },
    tokenExp :{
        type: Number
    },
    live_on:{
        type: Number
    },
    livestream_key: {
        type: String
    },
    livestream_title:{
        type: String
    },
    livestream_description:{
        type: String
    },
    livestream_room:{
        type: Number
    },
    livestream_createdAt:{
        type:Date
    }
})


const User = mongoose.model('User', userSchema);

module.exports = { User }