const { User } = require('../models/User');
var session_user = []
const mongoose = require("mongoose");
const { v4: uuidv4 } = require('uuid');
const jwt = require('jsonwebtoken');
let auth = (req, res, next) => {
  // let token = req.cookies.w_auth;
  // console.log(token);
  // User.findByToken(token, (err, user) => {
  //   if (err) throw err;
  //   if (!user)
  //     return res.json({
  //       isAuth: false,
  //       error: true
  //     });
  //
  //   session_user = user;
  //   req.token = token;
  //   req.user = user;
  //   next();
  // });
  let token = req.get('Authorization')
  token = token.substring(7);
  var decoded = jwt.decode(token);
  if(!decoded) {
    return res.json({
      isAuth: false,
      error: true
    });
  }
  var user_id = decoded.sub;
  User.findOne({
    _id: mongoose.Types.ObjectId(user_id)
  },(err, user)  => {
    if (err) throw err;
    if (!user)
      return res.json({
        isAuth: false,
        error: true
      });
      if(!user.livestream_key){
        user.livestream_key =  uuidv4().toString();
        user.save();
      }
    session_user = user;  
    req.user = user;
    next();
  })
};

module.exports = { auth,session_user };
