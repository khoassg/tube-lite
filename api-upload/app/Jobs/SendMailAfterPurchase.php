<?php

namespace App\Jobs;

use App\Mails\MailDetail;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendMailAfterPurchase extends Facade
{
    protected $params;
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        Log::debug('Begin Job');
        $mail=new MailDetail($this->params['mailParams']);
        $mail = $mail->build();
        Mail::to($this->params['email'])->send($mail);
        //Todo here
        Log::debug('End Job');

        return true;
    }
    public static function getFacadeAccessor()
    {
        return 'SendMailAfterPurchase';
    }
}
