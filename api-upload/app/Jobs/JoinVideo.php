<?php

namespace App\Jobs;

use App\Api\Entities\Video;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Illuminate\Support\Facades\Log;

class JoinVideo extends Job
{
    /**
     * Create a new job instance.
     */
    protected $params;
    public function __construct($params){
        $this->params = $params;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        Log::debug('Begin Job');
        $ffmpeg = FFMpeg::create();
        $video = $ffmpeg->open($this->params['first_video']);
        $video->concat($this->params['array_video_paths'])->saveFromSameCodecs($this->params['final_video'],TRUE);
        $video->
        $frame = $video->frame(TimeCode::fromSeconds(1));
        $frame->save($this->params['thumbnail_path']);
        Video::create([
            'title' => $this->params['title'],
            ''
        ])
        //Todo here
        Log::debug('End Job');

        return true;
    }
}
