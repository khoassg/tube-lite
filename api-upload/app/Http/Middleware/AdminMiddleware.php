<?php

namespace App\Http\Middleware;

use App\Api\Entities\User;
use App\Api\Repositories\Contracts\UserRepository;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Support\Facades\Log;

class AdminMiddleware
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository,Auth $auth)
    {
        $this->userRepository = $userRepository;
        $this->auth = $auth;
    }

    protected $auth;
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
//        // Validate password.
//        $user_id = $request->header('userId');
//        $user = User::where('_id',mongo_id($user_id))->first();
//        if(empty($user)){
//            return response(['message'=>'Unauthorized.','status_code'=>401], 401);
//        }
//
//        return $next($request);
        if ($this->auth->guard($guard)->guest()) {
            return response(['message'=>'Unauthorized.','status_code'=>401], 401);
        }

        return $next($request);
    }
}
