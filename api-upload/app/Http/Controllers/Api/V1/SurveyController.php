<?php


namespace App\Http\Controllers\Api\V1;


use App\Api\Entities\Survey;
use App\Api\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SurveyController extends Controller
{
    protected $auth;
    protected $request;

    public function __construct(
        Request $request,
        AuthManager $auth
    )
    {
        $this->auth = $auth;
        $this->request = $request;
    }
    public function listQuestion(){
        $type = $this->request->get('type');
        $data = [];
        if($type == 'video'){
            foreach(Survey::VIDEO_QUESTIONS as $index => $question){
                $data[] = [
                    'id' => $index,
                    'content' => $question['text'],
                    'type' => $question['type'],
                    'description' => $question['explain_text']
                ];
            }
        }
        else{
            foreach(Survey::COMMENT_QUESTIONS as $index => $question){
                $data[] = [
                    'id' => $index,
                    'content' => $question['text'],
                    'type' => $question['type'],
                    'description' => $question['text']
                ];
            }
        }
        return $this->successRequest($data);
    }
    public function addReport(){
        $session_user_id = Auth::getPayLoad()->get('sub');
        $source = $this->request->get('source');
        $object_id = $this->request->get('object_id');
        $content = $this->request->get('content');
        $type = $this->request->get('type');
        $attribute = [
            'source' => $source,
            'type' => $type,
            'object_id' => mongo_id($object_id),
            'report_user_id' => mongo_id($session_user_id),
            'content' => $content
        ];
        $survey = Survey::create($attribute);
        return $this->successRequest($survey->transform());
    }
}