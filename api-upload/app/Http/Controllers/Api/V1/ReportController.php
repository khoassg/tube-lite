<?php


namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Comment;
use App\Api\Entities\Like;
use App\Api\Entities\Video;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    protected $auth;
    protected $request;

    public function __construct(
        Request $request,
        AuthManager $auth
    )
    {
        $this->auth = $auth;
        $this->request = $request;
    }

    public function report(){
        $validator = \Validator::make($this->request->all(), [
            'from_date' => 'date_format:Y-m-d',
            'to_date' => 'date_format:Y-m-d',   
        ]);        
        if($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }


        $user_id = Auth::getPayLoad()->get('sub');
//        $actions = Like::where('owner_id',mongo_id($user_id));
        $videos = Video::where('writer',mongo_id($user_id))->paginate();
        $data = [
            'meta' => build_meta_paging($videos),
            'items' => [],
        ];
        foreach($videos as $video){
            $actions = Like::where('owner_id',mongo_id($video->writer));
            if(!empty($this->request->get('from_date'))){
                $from_date = Carbon::parse($this->request->get('from_date'));
                $actions = $actions->where('createdAt','>=',$from_date->startOfDay());
            }
            if(!empty($this->request->get('to_date'))){
                $to_date = Carbon::parse($this->request->get('to_date'));
                $actions = $actions->where('createdAt','<=',$to_date->endOfDay());
            }
            $actions = $actions->where('videoId',mongo_id($video->_id));
            $action_like  = (clone $actions)->where('type','like')->count();
            $action_dislike = (clone $actions)->where('type','dislike')->count();
            $action_view = (clone $actions)->where('type','view')->count();

            $comment_count = Comment::where('postId',mongo_id($video->_id));
            if(!empty($this->request->get('from_date'))){
                $from_date = Carbon::parse($this->request->get('from_date'));
                $comment_count = $comment_count->where('createdAt','>=',$from_date->startOfDay());
            }
            if(!empty($this->request->get('to_date'))){
                $to_date = Carbon::parse($this->request->get('to_date'));
                $comment_count = $comment_count->where('createdAt','<=',$to_date->endOfDay());
            }
            $comment_count = $comment_count->count();
            $data['items'][] = [
                'label' => $video->title,
                'views' => $action_view,
                'likes' => $action_like,
                'dislikes' => $action_dislike,
                'comments' => $comment_count
            ];
//            $data = [];
//            $video_index = [];

//            foreach($actions as $action){
//                if(!isset($data[mongo_id_string($action->videoId)][$action->type])){
//                    $data[mongo_id_string($action->videoId)][$action->type] = 1;
//                }
//                else{
//                    $data[mongo_id_string($action->videoId)][$action->type] ++;
//                }
////                if(empty($video_index[mongo_id_string($action->videoId)])){
////                    $comment_count = Comment::where('postId',mongo_id($action->videoId));
////                    if(!empty($this->request->get('from_date'))){
////                        $from_date = Carbon::parse($this->request->get('from_date'));
////                        $comment_count = $comment_count->where('createdAt','>=',$from_date->startOfDay());
////                    }
////                    if(!empty($this->request->get('to_date'))){
////                        $to_date = Carbon::parse($this->request->get('to_date'));
////                        $comment_count = $comment_count->where('createdAt','<=',$to_date->endOfDay());
////                    }
////                    $data[mongo_id_string($action->videoId)]['comment'] = $comment_count->count();
////                }
//            }
        }
        return $this->successRequest($data);
    }
}