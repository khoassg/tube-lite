<?php


namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Libraries\Gma\Api\ApiNotification;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    protected $request;

    protected $auth;

    function __construct(AuthManager $auth,
                         Request $request)
    {
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }

    public function listNotification()
    {
        $source = 'all';
        $type = [];
        if (!empty($this->request->get('type'))) {
            $type = $this->request->get('type');
        }
        if (!empty($this->request->get('source'))) {
            $source = $this->request->get('source');
        }
        if($source == 'task'){
            $this->resetCount();
        }
        return ApiNotification::listNotification($source, $type, $option = ['is_paginate' => 1, 'limit' => 15]);
    }

    public function maskNotification()
    {
        $validator = \Validator::make($this->request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        return ApiNotification::maskAsRead($id = $this->request->get('id'), $user_id = $this->request->header('userId'));
    }

    public function delete()
    {
        $validator = \Validator::make($this->request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        return ApiNotification::deleteNotification($id = $this->request->get('id'), $user_id = $this->request->header('userId'));
    }

    public function maskNotificationAll()
    {
        return ApiNotification::maskAsReadAll($user_id = $this->request->header('userId'));
    }

    public function archiveNotification()
    {
        $validator = \Validator::make($this->request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        return ApiNotification::archiveNotification($id = $this->request->get('id'), $user_id = $this->request->header('userId'));
    }

    public function unarchiveNotification()
    {
        $validator = \Validator::make($this->request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        return ApiNotification::unarchiveNotification($id = $this->request->get('id'), $user_id = $this->request->header('userId'));
    }

    public function unReadNotification()
    {
        $validator = \Validator::make($this->request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        return ApiNotification::unReadNotification($id = $this->request->get('id'), $user_id = $this->request->header('userId'));
    }

    public function confirmNotification()
    {
        $validator = \Validator::make($this->request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        return ApiNotification::confirmNotification($id = $this->request->get('id'), $user_id = $this->request->header('userId'));
    }

    public function listUserConfirm()
    {
        $validator = \Validator::make($this->request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        return ApiNotification::listUserConfirm($id = $this->request->get('id'));
    }

    public function list()
    {
        $source = 'all';
        $type = [];
        if (!empty($this->request->get('type'))) {
            $type = $this->request->get('type');
        }
        if (!empty($this->request->get('source'))) {
            $source = $this->request->get('source');
        }
        $response = ApiNotification::listNotification($source, $type, $option = ['is_paginate' => 1, 'limit' => 15]);
        $data = [
            'items' => $response['item'],
            'meta' => $response['meta']
        ];
        return $this->successRequest($data);
    }

    public function countUnreadNoti()
    {
        $user_id = $this->request->header('userId');
        if (!empty($this->request->get('source'))) {
            $source = $this->request->get('source');
        } else {
            $source = "all";
        }

        return $this->successRequest(ApiNotification::countUnreadNoti($user_id, $source));
    }
    public function resetCount()
    {
        $user_id = $this->request->header('userId');
        if (!empty($this->request->get('source'))) {
            $source = $this->request->get('source');
        } else {
            $source = "all";
        }
        ApiNotification::resetCount($user_id,$source);
        return $this->successRequest(trans('core.success'));
    }
}