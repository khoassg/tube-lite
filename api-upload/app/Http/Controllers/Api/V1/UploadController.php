<?php


namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    protected $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function upload(){
        $file_name = $this->request->get('file_name');
        $is_final = $this->request->get('is_final');
        $file = $this->request->file('file');
        $ext = $file->getClientOriginalExtension();
        if($ext != '.mp4'){
            return $this->errorBadRequest('Only mp4 file is allowed');
        }
        $date_string = Carbon::now()->toDateString();
        $directory_path = public_path('static').'/'.$date_string.'/';
        if (!is_dir($directory_path)){
            if (!mkdir($directory_path, 0755, true)) {
                return $this->errorBadRequest('Upload failed');
            }
        }
        $file_path = $directory_path.$file_name.$ext;
        $file->move($file_path);
        if($is_final){

        }
        return $this->successRequest('Success');
    }
}