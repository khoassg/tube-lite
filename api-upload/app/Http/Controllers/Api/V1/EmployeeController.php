<?php


namespace App\Http\Controllers\Api\V1;


use App\Api\Entities\User;
use App\Api\Entities\UserOTP;
use App\Http\Controllers\Controller;
use App\Libraries\Gma\Api\ApiOtp;
use App\Mails\SendOtp;
use Carbon\Carbon;
use Gma\CommonHelper;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{

    function __construct(AuthManager $auth,
                         Request $request)
    {
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }


    public function register(){
        $validator = \Validator::make($this->request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $secret = env('APP_KEY');
        $email = $this->request->get('email');
        $password = $this->request->get('password');
        if($password != $this->request->get('password')){
            return $this->errorBadRequest('Password must be the same as confirm password');
        }
        $hashed_password = hash_hmac('sha256', $password, $secret);
        $userCheck = User::where('email',$email)->first();
        if(!empty($userCheck)){
            return $this->errorBadRequest('Email has been used');
        }
        if(!empty($this->request->get('send_otp'))){
            $attribute = [
                'otp' => rand(11111,99999),
                'email' => $this->request->get('email'),
                'created_at' => mongo_date(Carbon::now()),
                'updated_at' => mongo_date(Carbon::now())
            ];
            $user_otp = UserOTP::create($attribute);
            $params = ['email_otp' => $attribute['otp'],
                'subject' => "TubeLite - {$attribute['otp']} is your verification code!"];
            $mail = new SendOtp($params);
            $delay =0;
            CommonHelper::sendMail($email, $mail, $delay);
            return $this->successRequest();
        }
        if(!empty($this->request->get('otp'))){
            $otp = $this->request->get('otp');
            $now = Carbon::now();
            $life_time = (clone $now)->subMinutes(UserOtp::LIFE_TIME_OTP);
            $otpCheck = UserOtp::where([
                'otp' => (int) $otp,
                'email' => $this->request->get('email')
            ])->where('created_at', '>=',$life_time)->first();
            if(empty($otpCheck)){
                return $this->errorBadRequest('Otp is not valid');
            }
            $attribute = [
                'name' => $this->request->get('name'),
                'name_nosign' => strtolower(remove_sign($this->request->get('name'))),
                'password' => $hashed_password,
                'email' => $this->request->get('email'),
                'created_at' => mongo_date(Carbon::now()),
                'updated_at' => mongo_date(Carbon::now()),
                'image' => $this->request->get('image')
            ];

            User::insert($attribute);
            $user = User::where('email',$this->request->get('email'))->first();
            $customClaims = [];
            $token = $this->auth->customClaims($customClaims)->fromUser($user);
            $data = [
                'token' => $token,
                'user' => $user->transform(),
            ];
            return $this->successRequest($data);
        }
        return $this->errorBadRequest('Otp missing');
    }
    public function send_otp(){
        $validator = \Validator::make($this->request->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $secret = env('APP_KEY');
        $email = $this->request->get('email');
        $attribute = [
            'otp' => rand(11111,99999),
            'email' => $this->request->get('email'),
            'created_at' => mongo_date(Carbon::now()),
            'updated_at' => mongo_date(Carbon::now())
        ];
        $user_otp = UserOTP::create($attribute);
        $params = ['email_otp' => $attribute['otp'],
            'subject' => "TubeLite - {$attribute['otp']} is your verification code!"];
        $mail = new SendOtp($params);
        $delay =0;
        CommonHelper::sendMail($email, $mail, $delay);
        return $this->successRequest();
    }
    public function reset(){
        $validator = \Validator::make($this->request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $secret = env('APP_KEY');
        $email = $this->request->get('email');
        $password = $this->request->get('password');
        if($password != $this->request->get('confirm_password')){
            return $this->errorBadRequest('Password must be the same as confirm password');
        }
        $hashed_password = hash_hmac('sha256', $password, $secret);
        if(!empty($this->request->get('otp'))){
            $otp = $this->request->get('otp');
            $now = Carbon::now();
            $life_time = (clone $now)->subMinutes(UserOtp::LIFE_TIME_OTP);
            $otpCheck = UserOtp::where([
                'otp' => (int) $otp,
                'email' => $this->request->get('email')
            ])->where('created_at', '>=',$life_time)->first();
            if(empty($otpCheck)){
                return $this->errorBadRequest('Otp is not valid');
            }
            $user = User::where('email',$this->request->get('email'))->first();
            if(empty($user)){
                return $this->errorBadRequest('User not found!');
            }
            $user->password = $hashed_password;
            $user->updated_at = mongo_date(Carbon::now());
            $customClaims = [];
            $token = $this->auth->customClaims($customClaims)->fromUser($user);
            $user->save();
            $data = [
                'token' => $token,
                'user' => $user->transform(),
            ];
            return $this->successRequest($data);
        }
        return $this->errorBadRequest('Otp missing');
    }
    public function login(){
        $email = (string)$this->request->get('email');
        $secret = env('APP_KEY');
        $password = $this->request->get('password');
        $hashed_password = hash_hmac('sha256', $password, $secret);
        $user = User::where('email',$email)->where('password',$hashed_password)->first();
        if(empty($user)){
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }
        $customClaims = [];
        $token = $this->auth->customClaims($customClaims)->fromUser($user);
        $data = [
            'token' => $token,
            'user' => $user->transform(),
        ];
        return $this->successRequest($data);
    }
}