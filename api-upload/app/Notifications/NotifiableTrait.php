<?php

namespace App\Notifications;

use App\Api\Repositories\Contracts\NotificationRepository;
use App\Api\Entities\Notification;
use App\Api\Entities\User;
use App\Libraries\Gma\Log;
use Carbon\Carbon;

trait NotifiableTrait
{
    /**
     * @var UserRepository
     */
    //protected $notificationRepository;

    /**
    * Onesignal URL
    *
    **/
    protected $_onesignalUrl = 'https://onesignal.com/api/v1/notifications/';



    /**
    * @Description: Send notification to database.
    * @param String content: notification's content
    * @param Array params: list para when client access detail data.
    * @param Array users: List user will recieve notification.
    * @param String thumbnail: Url thumbnail
    **/
    // in your notification
    public function toDatabase($params)
    {
        $attributes = [
            'content' => $params['content'],
            'data' => $params['data'],
            'users'   => $params['users'],
            'created_at' => mongo_date(Carbon::now()),
            'updated_at' => mongo_date(Carbon::now()),
        ];
        $notification = Notification::create($attributes);
        return $notification;
    }
    public function toOneSignal($params)
    {
        //Không có nội dung thì return false ngay.
        if (empty($params['content'])) {
            return false;
        }

        //Set limit time và limit memory.
        set_time_limit(0);
        ini_set("memory_limit", -1);

        $arrPush = [];

        // Set content cho notification.
        $arrPush['contents'] = ['en' => $params['content']];
        if(isset($params['multi_lang_contents'])){
            $arrPush['multi_lang_contents'] = $params['multi_lang_contents'];
        }
        if(isset($params['multi_lang_headings'])){
            $arrPush['multi_lang_headings'] = $params['multi_lang_headings'];
        }
        //Nếu có headings thì add headings vào
        if (!empty($params['heading'])) {
            $arrPush['headings'] = ['en' => $params['heading']];
        }

        if (!empty($params['data'])) {
            $arrPush['data'] = $params['data'];
        }
        // $arrPush['data'] = ['id' => $params['noti_id'],
        //                     'activity' => $params['activity'],
        //                     'thumbnail' => $params['thumbnail']
        //                     ];
        //Set sound for app
        $arrPush['ios_sound'] = 'notification.mp3';
        $arrPush['android_sound'] = 'notification';

        $arrPush['android_visibility'] = 1;
        //Hẹn giờ để gửi tin nhắn
        if (!empty($params['send_after'])) {
            $arrPush['send_after'] = $params['send_after'];
        }
        $response = [];
        if (count($params['users']) >= 1) {

            foreach ($params['users'] as $key => $value) {
                $arrPush['user_id'] = strval($value);
                $arrPush['ios_badgeType'] = "SetTo";
                $arrPush['ios_badgeCount'] = 1; //Total noti
                //var_dump($arrPush);return;
                $dataOneSignal = self::singlePushNotification($arrPush);
                if (!empty($dataOneSignal) && !empty($dataOneSignal['id'])) {
                    $response[$value] = $dataOneSignal['id'];
                }
            }
        } else {

            $arrPush['option']['ios_badgeType'] = "Increase";
            $arrPush['option']['ios_badgeCount'] = 1;
            $arrPush['option']['included_segments'] = array('All');
            $response = self::singlePushNotification($arrPush);
        }
        return $response;
    }
    /**
     * Push notificaiton sử dụng Onesignal
     * @param content: nội dung
     */
    public function singlePushNotification($arrParam)
    {
        if(empty($arrParam['user_id'])){
            return null;
        }
        //Onesignal Config
        // $onesignalConfig = config('services.onesignal');

        //Get current user's app
        $rest_api_key = 'YWEwZTEzNmYtNWExNi00MGUwLWFlOTQtZTRkYzk4OGNlZTky';
        $arrInfoPush = array();
        $arrInfoPush['app_id'] = '3e8a81fb-c43f-40e3-a0d2-1990834a4e26';
        if (!empty($arrParam)) {
            $arrInfoPush = array_merge($arrInfoPush, $arrParam);
        }
        if(!empty($content)){
            $arrInfoPush['contents'] = ['en' => $content];
        }
        if(!empty($heading)){
            $arrInfoPush['headings'] = ['en' => $heading];
        }
        //var_dump($arrInfoPush);return;
        if ($arrParam['user_id']) {
            $arrInfoPush['tags'][] = array('key' => 'user_id', 'relation' => '=', 'value' => mongo_id_string($arrParam['user_id']));
        }
        unset($arrInfoPush['multi_lang_contents']);
        unset($arrInfoPush['multi_lang_headings']);
        //unset($arrInfoPush['data']);
        $arrInfoPush = json_encode($arrInfoPush);
        //echo $arrInfoPush,'<br>';return;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_onesignalUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            "Authorization: Basic {$rest_api_key}"
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arrInfoPush);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, 1);
        // echo "<pre>";
        return $response;
    }

}
