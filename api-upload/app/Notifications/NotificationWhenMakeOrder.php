<?php

namespace App\Notifications;

use App\Notifications\NotifiableTrait;

class NotificationWhenMakeOrder{
    use NotifiableTrait;
    /**
    * @var $param
    *
    **/
    protected $params;

    /**
    * @var $sendToDatabase
    **/
    protected $isDatabase = true;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct($params){
        $this->params = $params;
        // $this->params['activity'] = $this->activity;
    }

    public function send(){
        if( empty($this->params['content'])||!isset($this->params['staff_id'])){
            return false;
        }
        //Send noti to database
        if($this->isDatabase){
            $noti_id = $this->toDatabase($this->params);
        }


        return true;
    }
}
