<?php


namespace Gma\RabbitMQ;

use App\Api\V4\Entities\EmployeeTimeKeeper;
use App\Api\V4\Entities\ShopSetup;
use App\Api\V4\Entities\TimeKeeper;
use App\Api\V4\Entities\TimeKeeperAllLog;
use App\Api\V4\Entities\TimeKeeperErrorLog;
use App\Api\V4\Entities\TimeKeeperLog;
use App\Notifications\Notification;
use App\Notifications\SendWhenSubscribe;
use Gma\Api\ApiTelegram;
use Gma\RabbitMQ\RabittMQTraits;
use Carbon\Carbon;
use Dompdf\Exception;
use Gma\Api\ApiTimeKeeperLog;
use Gma\CommonHelper;
use Illuminate\Support\Facades\DB;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\Api\V4\Entities\Test;
use PhpAmqpLib\Wire\AMQPTable;

class RabittMQ
{
    use RabittMQTraits;
    /**
     * @param array $data
     * @param array $option
     * @description publish message to queue
     * @throws \Exception
     */
    public static function writeQueue($messages = [], $option = [])
    {
        try{
            $queue_name = 'default';
            if(!empty($option['queue_name'])){
                $queue_name = $option['queue_name'];
            }
            if(!empty($messages)){
                $connection = self::openConnection();
                if(!empty($connection)){
                    $channel = $connection->channel();
                    $queue_exchange = $queue_name.'.exchange';
                    $publish_exchange = $queue_exchange;
                    //Nếu có delay
                    /*
                     * B1 : tạo 1 queue với tên là queueName.sogiaydelay, queue đó có các args bao gồm:
                     *   Thời gian msg tồn tại (thời gian delay)
                     *   Thời gian mà queue đó sẽ tồn tại (sau khi thực hiện xong thì xóa queue)
                     *   Exchange mà khi msg die sẽ gửi tới , ở đây sẽ là exchange của queue
                     *
                     */
                    if(!empty($option['delay'])) {
                        $delayQueue = $queue_name . '_delay' . $option['delay'];
                        $delay_exchange = $queue_exchange.'_delay.'.$option['delay'].'exchange';
                        $channel->exchange_declare($delay_exchange,'direct');
                        $channel->queue_declare(
                            $delayQueue,
                            false,
                            false,
                            false,
                            false,
                            true,
                            new AMQPTable(array(
                                'x-message-ttl' => $option['delay']*1000,
                                "x-expires" => $option['delay'] * 1000 + 10000,
                                'x-dead-letter-exchange' => $queue_exchange,
                            ))
                        );
                        $channel->queue_bind($delayQueue,$delay_exchange);
                        $publish_exchange = $delay_exchange;
                    }
                    // Trường hợp multi job
                    if(is_array($messages)) {
                        foreach($messages as $message){
                            $message = json_encode($message, true);
                            $pub_message = new AMQPMessage($message, array('content_type' => 'application/json', 'delivery_mode' => 2));
                            $channel->basic_publish($pub_message,$publish_exchange);
                        }
                    }
                    // Trường hợp single job
                    else {
                        $message = $messages;
                        $message = json_encode($message, true);
                        $pub_message = new AMQPMessage($message, array('content_type' => 'application/json', 'delivery_mode' => 2));
                        $channel->basic_publish($pub_message,$publish_exchange);
                    }
                    $channel->close();
                    $connection->close();
                }
            }
        }
        catch(\Exception $exception){
            Log::debug($exception);
        }
    }

    public static function readQueue($option = [])
    {
        try {
            $connection = self::openConnection();
            if (!empty($connection)) {
                $channel = $connection->channel();
                $queue_name = $option['queue_name'];
                $queue_exchange = $queue_name . '.exchange';
                $args = [
                    'priority' => 10, // maximum priority for message
                ];
                $try_times = $option['tries'];
                //Khai báo để ở dưới check xem msg từ delay hay từ retry
                $retry_queue_name = $queue_name . '_retry';
                //Add property x-dead-letter-exchange to queue
                $retry_exchange = $queue_name . '_retry.exchange';
                $args['x-dead-letter-exchange'] = $retry_exchange;
                $retry_args = [
                    'x-message-ttl' => $option['delay_retry'] * 1000,
                    'x-dead-letter-exchange' => $queue_exchange
                ];
                $channel->exchange_declare($retry_exchange, 'direct');
                $channel->queue_declare($retry_queue_name, false, true, false, false, '', new AMQPTable($retry_args));
                $channel->queue_bind($retry_queue_name, $retry_exchange);
                //Tạo exchange
                $channel->exchange_declare($queue_exchange, 'direct');
                //Tạo queue
                $channel->queue_declare($queue_name, false, true, false, false, '', new AMQPTable($args));

                //Bind exchange và queue với nhau
                // Retryexchange bind với queue chính
                // exchange chính bind với queue retry
                $channel->queue_bind($queue_name, $queue_exchange);
                $callback = function ($msg) use ($queue_name, $try_times, $option, $retry_queue_name) {
                    $msgBody = json_decode($msg->body,true);
                    if (!empty($msgBody)) {
                        $start_time = microtime(true);
                        //Nhận data là json
                        if(is_array($msgBody)){
                            try{
                                switch($msgBody['type']){
                                    case 'send-notification':{
                                        //Register 1 cái signal để báo timeout, bắt chước Illuminate/Worker
                                        $params = [
                                            'users' => $msgBody['users'],
                                            'content' => $msgBody['content'],
                                        ];
                                        $notification = new SendWhenSubscribe($params);
                                        $notification->send();
                                        write_to_console('Proccessed: Send notification');
                                        //Chạy xong function rồi thì không gửi signal
                                        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                                        $end_time = microtime(true);
                                        break;
                                    }
                                    case 'generate-otp':

                                        break;
                                    default:break;
                                }
                                $execution_time = ($end_time - $start_time);
                                write_to_console("Execution time of script = " . $execution_time);
                                Log::debug("Execution time of script = " . $execution_time . " sec" . "\n");
                                if (!empty($option['sleep'])) {
                                    $sleep_time = round((float)$option['sleep'] - $execution_time);
                                    write_to_console('sleep time: '. $sleep_time);
                                    if(!$sleep_time > 0){
                                        write_to_console('sleep: '.$sleep_time);
                                        sleep($option['sleep']);
                                    }
                                }
                            }
                            catch (\Exception $exception){
                                Log::debug($exception);
                                djson($exception->getMessage());
                                if (!empty($option['sleep'])) {
                                    sleep($option['sleep']);
                                }
                                write_to_console('Proccessed Failed: '. $msgBody['type']);
                                //set false để không redeliver lại
                                $msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], false);
                                if (!empty($option['sleep'])) {
                                    sleep($option['sleep']);
                                }
                            }

                        }
                    }

                };
                //1 lần chỉ lấy 1 msg
                $channel->basic_qos(null, 1, null);
                $channel->basic_consume($queue_name, '', false, false, false, false, $callback);
                while ($channel->is_consuming()) {
                    $channel->wait();
                }

                $channel->close();
                $connection->close();
            }
        }
        catch(\Exception $exception){
            write_to_console('Khong catch dc exception timeout');
            write_to_console($exception->getMessage());
            self::report($exception);
        }
    }
}