<?php


namespace App\Libraries\Gma\Api;


use App\Api\Entities\UserOTP;
use App\Mails\SendOtp;
use Carbon\Carbon;
use Gma\CommonHelper;

class ApiOtp
{
    /**
     * Create Tanca OTP
     * @param $driver: email/sms
     * @param $driver: sms/email
     * @param $attributes['phone']
     * @param $attributes['email']
     * @param $attributes['user_id']
     * @return UserOtp Entity
     * */
    public static function createOtp($attributes, $type = 'login', $driver = 'sms') {
        // Tạo một OTP (trường hợp chưa có otp)
        if(empty($attributes['otp'])) {
            $otp = rand(11111,99999);
            $attributes['otp'] = $otp;
        }
        $attributes['type'] = $type;
        $attributes['driver'] = $driver;
        $otp = UserOTP::create($attributes);
        return $otp;
    }
    /**
     * Create OTP & send email to user.
     * @param $phone: Phone (+84909224002)
     * @param $email: User's Email
     * @param $type: Type (login, register)
     * @param $options: array (user_id)
     **/
    public static function createEmailOtp($phone, $email , $type = 'login', $options = []) {
        // Kiểm tra xem email của user có OK để gửi không: không phải dạng @tanca.io hoặc @greenapp.vn
        $domains = ['greenapp.vn', 'tanca.io', 'argi.com'];
        foreach($domains as $key => $dm) {
            $pos = strpos($email, $dm);
            if($pos !== false) {
                return 0;
            }
        }
        $attributes = [
            'phone' => $phone,
            'email' => $email,
        ];
        // Có user_id
        if(!empty($options['user_id'])) {
            $attributes['user_id'] = $options['user_id'];
        }
        $userOtp = self::createOtp($attributes, 'login', 'email');
        $otp = $userOtp->otp;

        // Gửi một email tới cho user
        $params = ['email_otp' => $otp,
            'subject' => "TANCA - {$otp} là mã xác thực để đăng nhập!"];
        $mail = new SendOtp($params);
        $delay =0;
        CommonHelper::sendMail($email, $mail, $delay);
        return 1;
    }

    /**
     * Confirm OTP
     * @param $otp: OTP code
     * @param $options: array(phone (+84909224002))
     **/
    public static function confirmOtp($otp, $options = []) {
        $params = [
            'otp' => (int)($otp),
            // 'shop_id' => null,
            // 'email' => null,
            // 'phone' => null,
            // 'user_id' => null
        ];

        // By shop_id
        if(!empty($options['shop_id'])) {
            $params['shop_id'] = mongo_id($options['email']);
        }

        // By email
        if(!empty($options['email'])) {
            $params['email'] = $options['email'];
        }
        // By Phone
        if(!empty($options['phone'])) {
            $params['phone'] = $options['phone'];
        }

        // By User_id
        if(!empty($options['user_id'])) {
            $params['user_id'] = $options['user_id'];
        }
        $now = Carbon::now();
        $life_time = (clone $now)->subMinutes(UserOtp::LIFE_TIME_OTP);
        $otpCheck = UserOtp::where($params)->where('created_at', '>=',$life_time)->first();
        if(empty($otpCheck)) {
            return 0;
        } else {
            $otpCheck->is_confirm = 1;
            $otpCheck->save();
            return 1;
        }
    }
}