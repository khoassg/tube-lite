<?php


namespace App\Libraries\Gma\Api;

use App\Api\Entities\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiNotification
{
    public static function listNotification($source = 'all',$type = [], $option = ['is_paginate' => 1, 'limit' => 15])
    {
        $user_id = Auth::getPayLoad()->get('sub');
        $meta = [];
        $notificationsTrans = [];
        $notifications = Notification::where('users', $user_id);
        //Lấy count những thông báo chưa đọc
        $noti = clone $notifications;
        $noti = $noti->where('read_users', '!=',$user_id)->get();
        $notifications = $notifications->orderBy('createdAt', 'desc');
        if (empty($option['is_paginate'])) {
            $notifications = $notifications->get();
        } else {
            $notifications = $notifications->paginate($option['limit']);
            $meta = build_meta_paging($notifications);
            $meta['unread_count'] = $noti->count();
        }
        foreach ($notifications as $key=>$notification) {
            $notificationsTrans[$key] = $notification->transform('for-list');
        }
        return ['item' => $notificationsTrans, 'meta' => $meta];
    }
    public static function maskAsRead($id='',$user_id='')
    {
        $user_id = Auth::getPayLoad()->get('sub');
        $notification=Notification::where(['_id'=>mongo_id($id),'users'=>$user_id])
            ->first();
        if(empty($notification)){
            return ['error_code'=>400,'messages'=>trans('notification.notification_not_found')];
        }
        if(!empty($notification->read_users)){
            if(in_array($user_id,$notification->read_users)){
                $notification->pull('read_users',$user_id);
            }
        }
        else{
            $notification->push('read_users',$user_id);
        }
        $notification->save();
        return ['error_code'=>0,'messages'=>trans('core.success')];
    }
    public static function DeleteNotification($id='',$user_id='')
    {
        $user_id = Auth::getPayLoad()->get('sub');
        $notification=Notification::where(['_id'=>mongo_id($id),'users'=>$user_id])->first();
        if(empty($notification)){
            //return ['error_code'=>400,'messages'=>trans('notification.notification_not_found')];
        }
        $users=$notification->users;
        if (is_numeric($key = array_search($user_id, $users))) {
            unset($users[$key]);
            $notification->users=$users;
            $notification->save();
        }
        return ['error_code'=>0,'messages'=>trans('core.success')];
    }
    public static function maskAsReadAll($user_id='')
    {
        $user_id = Auth::getPayLoad()->get('sub');
        $notifications=Notification::where('users',$user_id)
            ->where('read_users','!=',$user_id)->get();
        if(empty($notifications)){
            return ['error_code'=>400,'messages'=>trans('notification.notification_not_found')];
        }
        foreach($notifications as $notification)
        {
            $notification->push('read_users',$user_id);
        }
        return ['error_code'=>0,'messages'=>trans('core.success')];
    }
    public static function countUnreadNoti($user_id = '',$source){
        $user_id = Auth::getPayLoad()->get('sub');
        $list_notifications_tasks = Notification::where(['users' => $user_id]);
        $list_notifications_tasks = $list_notifications_tasks->where(
            function ($query) use ($user_id){
                $query->where('read_users','!=',$user_id);
                $query->orWhere('read_users','==',null);
            })->where('is_clicked','!=',1)->count();
        return [
            'unread_count' => $list_notifications_tasks
        ];
    }
    public static function resetCount($user_id = '',$source){
        $user_id = Auth::getPayLoad()->get('sub');
        $list_notifications_tasks = Notification::where(['users' => $user_id]);
        if($source == "all"){
            $list_notifications_tasks->where('source','!=',null);
        }
        else{
            $list_notifications_tasks->where('source','=',$source);
        }
        $list_notifications_tasks = $list_notifications_tasks->where(
            function ($query) use ($user_id){
                $query->where('read_users','!=',$user_id);
                $query->orWhere('read_users','==',null);
            })->where('is_clicked','!=',1)->get();
        if(count($list_notifications_tasks) > 0){
            foreach($list_notifications_tasks as $list_notifications_task){
                $list_notifications_task->is_clicked = 1;
                $list_notifications_task->save();
            }
        }
    }
}