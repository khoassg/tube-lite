<?php

namespace App\Libraries\Gma\Api;
use Illuminate\Queue\Queue;

class ApiJob extends Queue
{

    public function createMsgPayLoad($job){
        $payload = $this->createPayload($job);
        return $payload;
    }
}
