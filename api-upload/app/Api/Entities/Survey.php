<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\SurveyTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Survey extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'surveys';

    protected $guarded = [];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    public const VIDEO_QUESTIONS = [
        [
            'text' => 'Sexual content',
            'type' => 'sexual_content',
            'explain_text' => 'Content that includes graphic sexual activity, nudity, or other types of sexual content',
        ],
        [
            'text' => 'Violent or repulsive content',
            'type' => 'violent_or_repulsive_content',
            'explain_text' => 'Content that is violent, graphic, or posted to shock viewers',
        ],
        [
            'text' => 'Hateful or abusive content',
            'type' => 'hateful_or_abusive_content',
            'explain_text' => 'Content that promotes hatred against protected groups, abuses vulnerable individuals, or engages in cyberbullying',
        ],
        [
            'text' => 'Harmful or dangerous acts',
            'type' => 'harmful_or_dangerous_acts',
            'explain_text' => 'Content that includes acts that may result in physical harm',
        ],
        [
            'text' => 'Child abuse',
            'type' => 'child_abuse',
            'explain_text' => 'Content that includes sexual, predatory, or abusive communications towards minors',
        ],
        [
            'text' => 'Promotes terrorism',
            'type' => 'promotes_terrorism',
            'explain_text' => 'Content that is intended to recruit for terrorist organizations, incite violence, glorify terrorist attacks, or otherwise promote acts of terrorism',
        ],
        [
            'text' => 'Spam or misleading',
            'type' => 'spam_or_misleading',
            'explain_text' => 'Content that is massively posted or otherwise misleading in nature',
        ],
        [
            'text' => 'Infringes my rights',
            'type' => 'infringes_my_rights',
            'explain_text' => 'Copyright, privacy, or other legal complaints',
        ]
    ];
    public const COMMENT_QUESTIONS =[
        [
            'text' => 'Unwanted commercial content or spam',
            'type' => 'unwanted_commercial_content_or_spam'
        ],
        [
            'text' => 'Pornography or sexually explicit material',
            'type' => 'pornography_or_sexually_explicit_material',
        ],
        [
            'text' => 'Child abuse',
            'type' => 'child_abuse'
        ],
        [
            'text' => 'Hate speech or graphic violence',
            'type' => 'hate_speech_or_graphic_violence',
        ],
        [
            'text' => 'Harassment or bullying',
            'type' => 'harassment_or_bullying',
        ]
    ];
    public function transform()
    {
        $transformer = new SurveyTransformer();

        return $transformer->transform($this);
    }

}
