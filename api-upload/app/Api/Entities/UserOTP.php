<?php

namespace App\Api\Entities;

use Api\Traits\ModelTrait;
use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UserOTPTransformer;
use Moloquent\Eloquent\SoftDeletes;

class UserOTP extends Moloquent
{
    use SoftDeletes;
    protected $collection = 'user_otps';
    const LIFE_TIME_OTP = 15; // Thời gian sử dụng của otp là 15 phút.
    /**
     * To make all fields fillable.
     */
    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform($type ='')
    {
        $transformer = new UserOTPTransformer();

        return $transformer->transform($this,$type);
    }

}
