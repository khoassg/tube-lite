<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\LikeTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Like extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'likes';

    protected $fillable = [];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new LikeTransformer();

        return $transformer->transform($this);
    }

}
