<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\CommentTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Comment extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'comments';

    protected $fillable = [];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new CommentTransformer();

        return $transformer->transform($this);
    }

}
