<?php

namespace App\Api\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UserTransformer;
use Moloquent\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Moloquent implements AuthenticatableContract, JWTSubject
{
    use Authenticatable;
	use SoftDeletes;

	protected $collection = 'users';

    protected $fillable = [];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    // jwt implementation
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function transform($type = '')
    {
        $transformer = new UserTransformer();

        return $transformer->transform($this,$type);
    }

}
