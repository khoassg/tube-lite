<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SurveyRepository
 */
interface SurveyRepository extends RepositoryInterface
{
    
}
