<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommentRepository
 */
interface CommentRepository extends RepositoryInterface
{
    
}
