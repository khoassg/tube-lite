<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NotificationRepository
 */
interface NotificationRepository extends RepositoryInterface
{
    
}
