<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VideoRepository
 */
interface VideoRepository extends RepositoryInterface
{
    
}
