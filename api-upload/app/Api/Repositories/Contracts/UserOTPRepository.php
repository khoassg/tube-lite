<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserOTPRepository
 */
interface UserOTPRepository extends RepositoryInterface
{
    
}
