<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\UserOTPRepository;
use App\Api\Entities\UserOTP;
use App\Api\Validators\UserOTPValidator;

/**
 * Class UserOTPRepositoryEloquent
 */
class UserOTPRepositoryEloquent extends BaseRepository implements UserOTPRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserOTP::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
