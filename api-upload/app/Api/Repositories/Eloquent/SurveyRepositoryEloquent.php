<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\SurveyRepository;
use App\Api\Entities\Survey;
use App\Api\Validators\SurveyValidator;

/**
 * Class SurveyRepositoryEloquent
 */
class SurveyRepositoryEloquent extends BaseRepository implements SurveyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Survey::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
