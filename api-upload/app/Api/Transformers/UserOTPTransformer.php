<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\UserOTP;

/**
 * Class UserOTPTransformer
 */
class UserOTPTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserOTP entity
     * @param \UserOTP $model
     *
     * @return array
     */
    public function transform(UserOTP $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
