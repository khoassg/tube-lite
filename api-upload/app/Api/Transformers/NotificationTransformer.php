<?php

namespace App\Api\Transformers;

use App\Api\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\Notification;

/**
 * Class NotificationTransformer
 */
class NotificationTransformer extends TransformerAbstract
{

    /**
     * Transform the \Notification entity
     * @param \Notification $model
     *
     * @return array
     */
    public function transform(Notification $model,$type)
    {
        $user_id = Auth::getPayLoad()->get('sub');
        if($type =='for-list'){
            $data = [
                'id'    => $model->_id,
                'image'   => $model->image,
                'title' => $model->content,
                'source'=> $model->source,
                'data'=>$model->data,
                'created_at'=> $model->createdAt->toDateTime()->setTimezone( new \DateTimeZone('Asia/Ho_Chi_Minh'))->format('Y-m-d H:i:s')

            ];
            if(!empty($model->read_users)){
                $read_users=$model->read_users;
                $flag=array_search($user_id, $read_users);
                if(!is_numeric($flag)){
                    $data['is_read']=0;
                }
                else{
                    $data['is_read']=1;
                }
            }
            else{
                $data['is_read']=0;
            }
            $created_user = User::where('_id', $model->created_user_id)->first();
            if (!empty($created_user)) {
                $data['created_user_obj'] = $created_user->transform('for-list');
            }
            return $data;
        }
    }
}
