<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\User;

/**
 * Class UserTransformer
 */
class UserTransformer extends TransformerAbstract
{

    /**
     * Transform the \User entity
     * @param \User $model
     *
     * @return array
     */
    public function transform(User $model,$type)
    {
        $data = array(
            'id' => $model->_id,
            'name' => $model->name,
            'image' => $model->image
        );
        return $data;
    }
}
