<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Like;

/**
 * Class LikeTransformer
 */
class LikeTransformer extends TransformerAbstract
{

    /**
     * Transform the \Like entity
     * @param \Like $model
     *
     * @return array
     */
    public function transform(Like $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
