<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Survey;

/**
 * Class SurveyTransformer
 */
class SurveyTransformer extends TransformerAbstract
{

    /**
     * Transform the \Survey entity
     * @param \Survey $model
     *
     * @return array
     */
    public function transform(Survey $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
