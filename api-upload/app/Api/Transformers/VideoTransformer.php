<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Video;

/**
 * Class VideoTransformer
 */
class VideoTransformer extends TransformerAbstract
{

    /**
     * Transform the \Video entity
     * @param \Video $model
     *
     * @return array
     */
    public function transform(Video $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
