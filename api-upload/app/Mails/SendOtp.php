<?php


namespace App\Mails;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOtp extends Mailable
{
    use Queueable, SerializesModels;
    protected $mailParam = [];
    public $subject = '';
    public function __construct($mailParam = array()){
        if(!empty($mailParam)){
            $this->mailParam = $mailParam;

            //Set subject if have param.
            if(!empty($this->mailParam['subject'])){
                $this->subject = $this->mailParam['subject'];
                unset($this->mailParam['subject']);
            }
        }
    }
    public function build()
    {
        $view = 'mail.tanca_email_otp-vi';
        return $this->view($view)
            ->subject($this->subject)
            ->with($this->mailParam);
    }
}