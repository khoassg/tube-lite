<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailNotify extends Mailable
{
    use Queueable, SerializesModels;

    /**
    * @var $mailParam: list param replace into mail template.
    * @var $mailParam['subject']: Title of email if need change.
    **/
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(){

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

    }
}
