<?php

namespace app\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Google\Cloud\ErrorReporting\Bootstrap;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $e
     */
    public function report(Exception $exception)
{
    if (isset($_SERVER['GAE_SERVICE'])) {
        Bootstrap::init();
        Bootstrap::exceptionHandler($exception);
    } else {
        parent::report($exception);
    }
}

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        return parent::render($request, $e);
    }
}
