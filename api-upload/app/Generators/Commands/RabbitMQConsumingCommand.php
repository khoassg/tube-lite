<?php


namespace App\Generators\Commands;

use Gma\RabbitMQ\RabittMQ;
use Illuminate\Console\Command;

class RabbitMQConsumingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:work                            
                            {--queue=default : The names of the queues to work}                                                       
                            {--delay=0 : The number of seconds to delay failed jobs}                                                        
                            {--sleep=3 : Number of seconds to sleep when no job is available}      
                            {--delay_retry=5 : The number of senconds to retry job after failed}
                            {--timeout=180 : Limit number of seconds for job to execute}                                                  
                            {--tries=0 : Number of times to attempt a job before logging it failed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start consuming messages on rabbtMQ queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $options = [
            'queue_name' => $this->option('queue'),
            'delay' => $this->option('delay'),
            'sleep' => $this->option('sleep'),
            'tries' => $this->option('tries'),
            'delay_retry' => $this->option('delay_retry'),
            'timeout' => $this->option('timeout')
        ];
        RabittMQ::readQueue($options);
    }
}