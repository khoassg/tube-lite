<?php
$data = [
    "driver" => env('MAIL_DRIVER'),
    "host" => env('MAIL_HOST'),
    "port" => env('MAIL_PORT'),
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'from' => ['address' => env('MAIL_USERNAME'), 'name' => 'QTV'],
    "username" => env('MAIL_USERNAME'),
    "password" => env('MAIL_PASSWORD'),
    "sendmail" => "/usr/sbin/sendmail -bs"
];
return $data;