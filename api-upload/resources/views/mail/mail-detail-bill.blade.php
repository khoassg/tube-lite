
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Hóa đơn điện tử.</title>
    <!-- <style> -->
  </head>
  <body>
    <span class="preheader"></span>
    <table class="body">
      <tr>
        <td class="center" align="center" valign="top">
          <center data-parsed="">
            
            
              <table align="center" class="container header float-center"><tbody><tr><td>
                <table class="row"><tbody><tr>
                  <th class="small-12 large-12">
                  <table>
                      <tr>
                        <td>
                        </td>
                      </tr>
                  </table>
                </th>
                </tr></tbody></table>
                <h2 style="text-align: center">CHI TIẾT HÓA ĐƠN</h2>
              </td></tr></tbody></table><table align="center" class="container float-center"><tbody><tr><td>
              <table class="row"><tbody><tr>
                <th class="small-12 large-12 columns first last"><table><tr><th>
                  <table class="spacer"><tbody><tr><td height="32px" style="font-size:32px;line-height:32px;">&#xA0;</td></tr></tbody></table> 
                    <table class="spacer"><tbody><tr><td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td></tr></tbody></table> 
                    <p>
                      <tr>
                        <th>Tên Giày</th>
                        <th>Size</th>
                        <th>Số lượng</th>
                        <th>Đơn giá</th>
                      </tr>
                    @foreach($product_name as $index => $item)
                      <tr>
                        <td>{{$item}}</td>
                        <td>{{$details[$index]['size']}}</td>
                        <td>{{$details[$index]['quantity']}}</td>
                        <td>{{$details[$index]['price']}}</td>
                      @endforeach

                    </p>
                    <p>Ngày tạo hóa đơn: <?php echo $created_at; ?></p>
                    <table class="row collapse footer">
                      <tbody>
                        <hr style="background: #cacaca !important">
                        <tr>
                          <th class="small-12 large-8 columns first">
                            <table>
                              <tr>
                                <th>
                                    <p>
                                        Tổng hóa đơn:{{$total}}
                                    </p>
                                </th>
                              </tr>
                            </table>
                          </th>
                          <th class="small-12 large-4 columns last">
                            <table>
                              <tr>
                                <th>
                                    <p class="text-right">
                                    </p>
                                </th>
                              </tr>
                            </table>
                          </th>
                        </tr>
                      </tbody>
                    </table>    </th></tr></table></th>
              </tr></tbody></table>
            </td></tr></tbody></table>
            <table class="spacer float-center"><tbody><tr><td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td></tr></tbody></table> 
          </center>
        </td>
      </tr>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none; white-space:nowrap; font:15px courier; line-height:0;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </body>
</html>