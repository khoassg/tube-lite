#!/usr/bin/env bash

set -e
role=$CONTAINER_ROLE
if [ "$role" = "queue" ]; then
    echo "This role is $role...."
    echo "Running the queue..."
    # Database queue
    # php /var/www/api/artisan queue:work --queue=highpriority --sleep=3 --tries=0 --timeout=600 &
    # php /var/www/api/artisan queue:work --queue=emails,default --sleep=60 --tries=300 --timeout=60 &
    # php /var/www/api/artisan queue:work --queue=shift1,shift2,shift3 --sleep=60 --tries=300 --timeout=0 &
    # php /var/www/api/artisan queue:work --queue=buildpayroll1,buildshift1,buildpayroll2,buildshift2,buildpayroll3,buildshift3 --sleep=30 --tries=100 --timeout=0 &

    # RabbitMQ queue
    php /var/www/api/artisan rabbitmq:work --queue=special --sleep=0 --tries=0 --timeout=180 &
    php /var/www/api/artisan rabbitmq:work --queue=high --sleep=10 --tries=0 --timeout=180 &
    php /var/www/api/artisan rabbitmq:work --queue=medium --sleep=60 --tries=0 --timeout=180 &
    php /var/www/api/artisan rabbitmq:work --queue=default --sleep=60 --tries=0 --timeout=180

elif [ "$role" = "schedule" ]; then
    echo "This role is $role...."
    echo "Running the schedule..."
    while [ true ]
    do
      php /var/www/api/artisan schedule:run --verbose --no-interaction &
      sleep 60
    done

else
    echo "That is api container"

    # apply environment variables to default.conf
    envsubst '$API_DOMAIN, $API_PORT' < /etc/nginx/conf.d/app.conf.template > /etc/nginx/conf.d/default.conf
    # Start supervisord and services (run php-fpm & nginx)
    exec /usr/bin/supervisord -n -c /etc/supervisord.conf
fi
