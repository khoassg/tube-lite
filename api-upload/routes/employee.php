<?php
$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->post('users/register','EmployeeController@register');
    $api->post('users/send_otp','EmployeeController@send_otp');
    $api->post('users/reset','EmployeeController@reset');
    $api->post('users/login','EmployeeController@login');
});
?>