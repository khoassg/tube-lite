<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.admin']], function ($api) {
        $api->get('survey/list-question',[
            'as'=>'SurveyController.listQuestion',
            'uses'=>'SurveyController@listQuestion',
        ]);
        $api->post('survey/add-report',[
            'as'=>'SurveyController.addReport',
            'uses'=>'SurveyController@addReport',
        ]);
    });
});