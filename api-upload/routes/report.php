<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.admin']], function ($api) {
        $api->get('report/report',[
            'as'=>'ReportController.report',
            'uses'=>'ReportController@report',
        ]);
    });
});