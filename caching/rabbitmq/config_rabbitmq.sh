#!/bin/bash

# This script needs to be executed just once
if [ -f /$0.completed ] ; then
  echo "$0 `date` /$0.completed found, skipping run"
  exit 0
fi

# Wait for RabbitMQ startup
for (( ; ; )) ; do
  sleep 5
  rabbitmqctl -q node_health_check > /dev/null 2>&1
  if [ $? -eq 0 ] ; then
    echo "$0 `date` rabbitmq is now running"
    break
  else
    echo "$0 `date` waiting for rabbitmq startup"
  fi
done

# Execute RabbitMQ config commands here

# Disable guest user
loopback_users = none

# Create User Admin
rabbitmqctl add_user ${RABBITMQ_ADMIN_USER} ${RABBITMQ_ADMIN_PASSWORD}
rabbitmqctl set_user_tags ${RABBITMQ_ADMIN_USER} administrator
echo "$0 `date` user ${RABBITMQ_ADMIN_USER} created"

# Create Tanca Queue Vhosts
rabbitmqctl add_vhost ${RABBITMQ_VHOST}
echo "$0 `date` vhosts ${RABBITMQ_VHOST} created"

# Create Tanca Queue User
rabbitmqctl add_user ${RABBITMQ_QUEUE_USER} ${RABBITMQ_QUEUE_PASSWORD}
echo "$0 `date` user ${RABBITMQ_QUEUE_USER} created"

# Add User Tanca Queue To Vhosts
rabbitmqctl set_permissions -p ${RABBITMQ_VHOST} ${RABBITMQ_QUEUE_USER} ".*" ".*" ".*"
echo "$0 `date` added ${RABBITMQ_QUEUE_USER} to vhosts ${RABBITMQ_VHOST}"

# Create mark so script is not ran again
touch /$0.completed

# END: Execute RabbitMQ config commands here