#!/usr/bin/env sh
set -eu

# apply environment variables to default.conf
envsubst '$API_DOMAIN, $CHAT_DOMAIN, $ADMIN_DOMAIN, $ROOT_DOMAIN, $TANCA_API, $TANCA_ADMIN_WEB, $TANCA_CHAT, $TANCA_QUEUE, $TANCA_SCHEDULE, $TANCA_RABBITMQ, $TANCA_REDIS, $API_PORT, $API_SSL_PORT, $ADMIN_PORT, $ADMIN_SSL_PORT, $CHAT_PORT, $CHAT_PORT' < /etc/nginx/conf.d/app.conf.template_notssl > /etc/nginx/conf.d/default.conf

############### CONFIG /ETC/NGINX/NGINX.CONF ##################
sed -i 's/.*worker_connections.*/worker_connections 50000;/g' /etc/nginx/nginx.conf
############### END CONFIG /ETCNGINX/NGINX.CONF ###############

exec nginx -g "daemon off;"

exec "$@"