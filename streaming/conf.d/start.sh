#!/usr/bin/env sh
set -eu
# exec /usr/local/nginx/sbin/nginx -t

exec /usr/local/nginx/sbin/nginx -g "daemon off;"

exec "$@"